<?php
/*
* @package		MijoShop
* @copyright	2009-2013 Miwisoft LLC, miwisoft.com
* @license		GNU/GPL http://www.gnu.org/copyleft/gpl.html
*/

// No Permission
defined('_JEXEC') or die ('Restricted access');

jimport('joomla.application.component.helper');

require_once JPATH_ADMINISTRATOR . '/components/com_finder/helpers/indexer/adapter.php';

class plgFinderMijoshop extends FinderIndexerAdapter {

	protected $context = 'Mijoshop';
	protected $extension = 'com_mijoshop';
	protected $layout = 'product';
	protected $type_title = 'Product';
	protected $table = '#__mijoshop_product_description';

	public function __construct(&$subject, $config) {
		parent::__construct($subject, $config);
		$this->loadLanguage();
		
		$file = JPATH_SITE.'/components/com_mijoshop/mijoshop/mijoshop.php';
		
		if (file_exists($file)) {
			require_once($file);
		}
	}

	public function onFinderAfterDelete($context, $table) {
        if ($context == 'com_mijoshop.product') {
            $id = $table;
        }
        elseif ($context == 'com_finder.index') {
            $id = $table->link_id;
        }
        else {
            return true;
        }
        
        return $this->remove($id);
	}

	public function onFinderAfterSave($context, $row, $isNew) {
        if ($context == 'com_mijoshop.product') {
            $this->reindex($row['id']);
        }

        return true;
	}

	public function onFinderChangeState($context, $pks, $value) {
        if ($context == 'com_mijoshop.category') {
            $this->categoryStateChange($pks, $value);
        }
	}

	protected function index(FinderIndexerResult $item, $format = 'html') {
        if (JComponentHelper::isEnabled($this->extension) == false) {
            return;
        }

        $registry = new JRegistry;
        $registry->loadString($item->metadata);
        $item->metadata = $registry;

        $item->addInstruction(FinderIndexer::META_CONTEXT, 'link');
        $item->addInstruction(FinderIndexer::META_CONTEXT, 'metakey');
        $item->addInstruction(FinderIndexer::META_CONTEXT, 'metadesc');

        $item->url = 'index.php?option=com_mijoshop&route=product/product&product_id='.$item->id;
        $item->route = $this->_getRoute($item);
        //$item->path = FinderIndexerHelper::getContentPath($item->route);
        $item->access = 1;

        $item->state = $this->translateState(intval($item->state));

        $item->addTaxonomy('Type', 'MijoShop Product');

        $manufacturer =$item->getElement('manufacturer_name');
        if (!empty($manufacturer)) {
            $item->addTaxonomy('MijoShop Manufacturer', $manufacturer);
        }

        $cats = self::getProductCategoryId($item->id);
        foreach($cats as $cat) {
            if (!empty($cat->name)){
                $item->addTaxonomy('MijoShop Category', $cat->name);
            }
        }

        FinderIndexerHelper::getContentExtras($item);

        if (method_exists('FinderIndexer', 'getInstance')) {
            FinderIndexer::getInstance()->index($item);
        }
        else {
            FinderIndexer::index($item);
        }
	}

	protected function setup() {
		return true;
	}

	protected function getListQuery($sql = null) {
        $db = JFactory::getDbo();

        $sql = is_a($sql, 'JDatabaseQuery') ? $sql : $db->getQuery(true);
        $sql->select('a.product_id as id, a.status AS state, a.date_added as start_date, a.product_id AS slug');
        $sql->select('pd.name as title, pd.description, pd.meta_keyword AS metakey, pd.meta_description AS metadesc');
        $sql->select('m.name As manufacturer_name');

        $sql->from('#__mijoshop_product AS a');
        $sql->join('LEFT', '#__mijoshop_product_description AS pd ON a.product_id = pd.product_id');
        $sql->join('LEFT', '#__mijoshop_manufacturer AS m ON m.manufacturer_id = a.manufacturer_id');


        return $sql;
	}

    protected function getItem($id) {
        JLog::add('FinderIndexerAdapter::getItem', JLog::INFO);

        $sql = $this->getListQuery();
        $sql->where('a.' . $this->db->quoteName('product_id') . ' = ' . (int) $id);

        $this->db->setQuery($sql);
        $row = $this->db->loadAssoc();

        if ($this->db->getErrorNum()) {
            throw new Exception($this->db->getErrorMsg(), 500);
        }

        $item = JArrayHelper::toObject($row, 'FinderIndexerResult');

        $item->type_id = $this->type_id;

        $item->layout = $this->layout;

        return $item;
    }

    protected function getProductCategoryId($id){
        $db = JFactory::getDbo();
		
        $sql = 'SELECT c.name FROM #__mijoshop_category_description AS c , #__mijoshop_product_to_category pc
                WHERE pc.category_id = c.category_id AND pc.product_id = '.$id;
        $db->setQuery($sql);
        $result = $db->loadObjectList();

        return $result;
    }

    protected function categoryStateChange($pks, $value) {
        if(!is_array($pks)){
            $pks = array($pks);
        }

        foreach($pks as $pk) {
            $query = $this->getStateQuery();
            $query->where('c.category_id = ' . (int) $pk);

            $this->db->setQuery($query);

            $items = $this->db->loadObjectList();

            foreach ($items as $item) {
                if ($value !== null) {
                    $temp = intval($value);
                }
                else {
                    $temp = intvall($item->state);
                }

                $this->change($item->id, 'state', $temp);

                $this->reindex($item->id);
            }
        }
    }

    protected function getStateQuery() {
        $query = $this->db->getQuery(true);

        $query->select('p.product_id AS id');
        $query->select('p.status AS state');
        $query->select('c.status AS cat_state');
        $query->from('#__mijoshop_category AS c');
        $query->leftJoin('#__mijoshop_product_to_category AS pc ON pc.category_id = c.category_id');
        $query->leftJoin('#__mijoshop_product AS p ON p.product_id = pc.product_id');
        $query->where('( SELECT COUNT( x.product_id ) FROM #__mijoshop_product_to_category AS x WHERE x.product_id = pc.product_id ) = 1 ');

        return $query;
    }

    protected function _getRoute($item) {
        $url = str_replace('index.php?', '', $item->url);
        parse_str($url, $vars);

        $id = 0;

        $route = $item->url;

        if (!isset($vars['view']) and !isset($vars['route'])) {
            $view = 'home';
        }

        if (isset($vars['route'])) {
            if ($vars['route'] == 'product/category') {
                $path_array = explode('_', $vars['path']);
                $id = end($path_array);
            }
            elseif ($vars['route'] == 'product/product') {
                $id = $vars['product_id'];
            }
            elseif ($vars['route'] == 'product/manufacturer/info') {
                $id = $vars['manufacturer_id'];
            }
            elseif ($vars['route'] == 'information/information') {
                $id = $vars['information_id'];
            }

            $view = MijoShop::get('router')->getView($vars['route']);
        }

        $Itemid = MijoShop::get('router')->getItemid($view, $id);

        if (!empty($Itemid)) {
            $route .= '&Itemid='.$Itemid;
        }

        return $route;
    }
}