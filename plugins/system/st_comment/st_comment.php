<?php
/**
 * @version		1.0.0
 * @copyright 	Beautiful-Templates.com
 * @author		Neo
 * @link		http://www.beautiful-templates.com
 * @license		License GNU General Public License version 2 or later http://www.gnu.org/licenses/gpl-2.0.html
 */

// no direct access
defined('_JEXEC') or die;

jimport('joomla.plugin.plugin');
class plgSystemST_Comment extends JPlugin
{
	public function __construct(& $subject, $config)
	{
	        parent::__construct($subject, $config);
	        $language = JFactory::getLanguage();
			$language->load('st_comment', JPATH_ADMINISTRATOR, 'en-GB', true);
			$language->load('st_comment', JPATH_ADMINISTRATOR, null, true);
	}
	
	function onContentAfterTitle($context, &$row, &$params, $page = 0)
	{
		$option = JRequest::getCmd('option');
		
		if($option == 'com_content'){
			$this->renderDisqus($row, $params, $page = 0);
		}
		return;
		
	}
	function renderDisqus(&$row, &$params, $page){
		
		$shortname = $this->params->get('shortname', '');
		
		if ($shortname == '') {
			return;
		}
		
		// API
		$mainframe = JFactory::getApplication();
		$document = JFactory::getDocument();
		$user = JFactory::getUser();

		// Assign paths
		$sitePath = JPATH_SITE;
		$siteUrl = JURI::root(true);

		// Requests
		$option = JRequest::getCmd('option');
		$view = JRequest::getCmd('view');
		$layout = JRequest::getCmd('layout');
		$page = JRequest::getCmd('page');
		$secid = JRequest::getInt('secid');
		$catid = JRequest::getInt('catid');
		$itemid = JRequest::getInt('Itemid');
		if (!$itemid)
			$itemid = 999999;
		
		$select_categories = $this->params->get('select_categories', '');
		$select_menus = $this->params->get('select_menus', '');
		
		// Get the current category
		$currectCategory = $row->catid;

		// Define plugin category restrictions
		$select_categories = (array)$select_categories;
		if (sizeof($select_categories) == 1 && $select_categories[0] == '')
		{
			$categories[] = $currectCategory;
		}
		else
		{
			$categories = $select_categories;
		}

		// Define plugin menu restrictions
		$select_menus = (array)$select_menus;
		
		if (sizeof($select_menus) == 1 && $select_menus[0] == '')
		{
			$menus[] = $itemid;
		}
		else
		{
			$menus = $select_menus;
		}
		
		
		$websiteURL = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != "off") ? "https://" . $_SERVER['HTTP_HOST'] : "http://" . $_SERVER['HTTP_HOST'];
		
		$levels = $user->getAuthorisedViewLevels();
		if (in_array($row->access, $levels))
		{
			if ($view == 'article')
			{
				$itemURL = explode("#", $_SERVER['REQUEST_URI']);
				$itemURL = $itemURL[0];
			} else
			{
				$itemURL = JRoute::_(ContentHelperRoute::getArticleRoute($row->id . ':' . $row->alias, $row->catid));
			}
		}
		
		$url = $websiteURL.$itemURL;
		$disqusIdentifier = substr(md5($shortname), 0, 10) . '_id' . $row->id ;
		
		//Check Menu and Category
		if(in_array($currectCategory, $categories) && in_array($itemid, $menus) && $option == 'com_content'){
			$html = '<div class="st_comment_counter">
					<script  type="text/javascript">
					var disqus_shortname = \'' . $shortname . '\';
					(function () {
						var s = document.createElement(\'script\'); s.async = true;
						s.type = \'text/javascript\';
						s.src = \'http://' . $shortname . '.disqus.com/count.js\';
						(document.getElementsByTagName(\'HEAD\')[0] || document.getElementsByTagName(\'BODY\')[0]).appendChild(s);
						}());
					</script>
					<a class="st_comment_link" href="'.$url.'#disqus_thread"
					data-disqus-identifier="'.$disqusIdentifier.'">Add a comment</a>
					</div>
					';
		}
		
		//show counter in article
		if($this->params->get('article_counter',1)){
			$row->text = $html.$row->text;
		}
		
		//show counter in categories
		if($this->params->get('categories_counter',1)){
			$row->introtext = $html.$row->introtext;
		}
	}
	public function onContentAfterDisplay($context, &$article, &$params, $limitstart=0)
	{
		$app = JFactory::getApplication();
		if ($app->isAdmin()) {
			return;
		}
		$option = JRequest::getCmd('option');
		if($option!='com_content'){
			return;
		}
		$shortname = $this->params->get('shortname', '');
		
		if ($shortname == '') {
			return;
		}
		
		// API
		$mainframe = JFactory::getApplication();
		$document = JFactory::getDocument();
		$user = JFactory::getUser();

		// Assign paths
		$sitePath = JPATH_SITE;
		$siteUrl = JURI::root(true);

		// Requests
		//$option = JRequest::getCmd('option');
		$view = JRequest::getCmd('view');
		$layout = JRequest::getCmd('layout');
		$page = JRequest::getCmd('page');
		$secid = JRequest::getInt('secid');
		$catid = JRequest::getInt('catid');
		$itemid = JRequest::getInt('Itemid');
		if (!$itemid)
			$itemid = 999999;
		
		$select_categories = $this->params->get('select_categories', '');
		$select_menus = $this->params->get('select_menus', '');
		
		// Get the current category
		$currectCategory = $article->catid;

		// Define plugin category restrictions
		$select_categories = (array)$select_categories;
		if (sizeof($select_categories) == 1 && $select_categories[0] == '')
		{
			$categories[] = $currectCategory;
		}
		else
		{
			$categories = $select_categories;
		}

		// Define plugin menu restrictions
		$select_menus = (array)$select_menus;
		
		if (sizeof($select_menus) == 1 && $select_menus[0] == '')
		{
			$menus[] = $itemid;
		}
		else
		{
			$menus = $select_menus;
		}
		
		if (in_array($currectCategory, $categories) && in_array($itemid, $menus) && $context == 'com_content.article') 
		{
			$html = '<div id="disqus_thread"></div>
    				<script type="text/javascript">
        				var disqus_shortname = "'.$shortname.'";
						(function() {
				            var dsq = document.createElement("script"); dsq.type = "text/javascript"; dsq.async = true;
				            dsq.src = "http://beautifultemplates.disqus.com/embed.js";
				            (document.getElementsByTagName("head")[0] || document.getElementsByTagName("body")[0]).appendChild(dsq);
				        })();
				    </script>';
			
			return $html;
		}
	}
}
