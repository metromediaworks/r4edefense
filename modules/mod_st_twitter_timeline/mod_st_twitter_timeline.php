<?php
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

$header = ($params->get('show_header')==0) ? " noheader" :"";
$scrollbar = ($params->get('show_scrollbar')==0) ? " noscrollbar" :"";
$footer = ($params->get('show_footer')==0) ? " nofooter" :"";
$borders = ($params->get('show_borders')==0) ? " noborders" :"";

?>

<a 
	class="twitter-timeline" 
	href="<?php echo $params->get('link'); ?>" 
	width="<?php echo $params->get('width');?>"
	height="<?php echo $params->get('height');?>"
	data-widget-id="<?php echo $params->get('widget_id'); ?>"
	data-chrome=" transparent <?php echo $header.$scrollbar.$footer.$borders ; ?>"
	data-tweet-limit="<?php echo $params->get('limit',3); ?>"
	data-theme="<?php echo $params->get('theme'); ?>"
	data-screen-name="<?php echo $params->get('twitter_name');?>"
	data-link-color="#fff"
	>Tweets by @minhkhmt1k3
</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
