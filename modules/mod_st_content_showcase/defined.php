<?php
/**
 * @copyright	submit-templates.com
 * @license		GNU General Public License version 2 or later;
 */

// no direct access
defined('_JEXEC') or die;

define('ST_NAME', 'mod_st_content_showcase');
define('ST_DIR', JPATH_ROOT.'/'.'modules'.'/'.ST_NAME.'/');
define('ST_MODEL_PATH', 'models'); 
define('ST_MODEL_PREFIX', 'stContentShowcaseModel');
define('ST_DIR_EXTS', ST_DIR.'exts'.'/');
define('ST_DIR_MODELS', ST_DIR.'models/');
define('ST_DIR_LAYOUTS', ST_DIR.'layouts/');

