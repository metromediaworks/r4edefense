<?php  

$classImage = '';
if ($params->get('image', 1) && isset($item->image_intro) and !empty($item->image_intro)) : 
	$classImage = 'image';
?>
<div class="media">
	<img src="<?php echo htmlspecialchars($item->image_intro); ?>"  alt="<?php echo htmlspecialchars($item->title); ?>"/>
	<div class="links">
		<a class="image fa fa-picture-o" rel="<?php echo $id; ?>" title="<?php htmlspecialchars($item->title) ?>" href="<?php echo $item->image_large; ?>"></a>
		<a class="link fa fa-link" href="<?php echo $item->link; ?>"></a>
		
	</div>
</div>
<?php endif; ?>