<?php if ($params->get('image')): ?>
<div class="image">
	<?php switch($params->get('image_link')){
		case 0: ?>
			<img src="<?php echo htmlspecialchars($item->image_intro); ?>"  alt="<?php echo htmlspecialchars($item->title); ?>"/>
		<?php break; case 1:?>
		<a  href="<?php echo $item->link	; ?>" title="<?php echo htmlspecialchars($item->title); ?>">
			<img src="<?php echo htmlspecialchars($item->image_intro); ?>"  alt="<?php echo htmlspecialchars($item->title); ?>"/>
		</a>
		<?php break; case 2:?>
		<a   href="<?php echo $item->image_large	; ?>" title="<?php echo htmlspecialchars($item->title); ?>">
			<img src="<?php echo htmlspecialchars($item->image_intro); ?>"  alt="<?php echo htmlspecialchars($item->title); ?>"/>
		</a>
	<?php break; }; ?>
</div>
<?php endif; ?>