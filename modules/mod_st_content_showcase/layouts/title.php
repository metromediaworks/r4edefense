<?php if ($params->get('title', 1)): ?>
	<h3 class="title">
	<?php if ($params->get('title_link', 1)): ?>
	<a href="<?php echo $item->link;?>">
			<?php echo $item->title;?></a>
	<?php else : ?>
		<?php echo $item->title; ?>
	<?php endif; ?>
	</h3>
<?php endif ?>