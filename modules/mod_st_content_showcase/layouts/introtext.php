<?php if ($params->get('introtext', 1)): ?>
	<div class="description"><?php echo ($params->get('introtext_length', 0) > 0) ? substr($item->introtext, 0 , $params->get('introtext_length')) : $item->introtext; ?></div>
<?php endif; ?>	