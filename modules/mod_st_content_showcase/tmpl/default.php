<?php
/**
 * @copyright	submit-templates.com
 * @license		GNU General Public License version 2 or later;
 */

// no direct access
defined('_JEXEC') or die;
$document = JFactory::getDocument();

if ($params->get('awesomefont', 0) > 0) {
	$document->addStyleSheet("modules/mod_st_content_showcase/assets/css/font-awesome.min.css");	
}

if (version_compare(JVERSION, '3.0.0','>') && $params->get('jquery', 0) > 0) {
	$document->addScript('http://code.jquery.com/jquery-1.8.3.js');
	$document->addScript('modules/mod_st_content_showcase/assets/js/noconflict.js');
} else if (version_compare(JVERSION, '3.0.0','<')) {
	$document->addScript('http://code.jquery.com/jquery-1.8.3.js');
	$document->addScript('modules/mod_st_content_showcase/assets/js/noconflict.js');
}

jimport( 'joomla.filesystem.folder' );

$exts = JFolder::folders(ST_DIR_EXTS);

foreach ($exts as $ext) 
{
	if ($params->get('extension') == $ext) 
	{
		$viewFile = ST_DIR_EXTS.DIRECTORY_SEPARATOR.$ext.DIRECTORY_SEPARATOR.'site'.DIRECTORY_SEPARATOR.'default.php';
		if(is_file($viewFile)) {
			require $viewFile;
		}	
	} 
}
?>
<?php if ($params->get('copyright', 1)) { ?>
<div class="copyright" style="margin: 5px; clear:both; text-align: center;">
	Beautiful-Templates.com
	<a href="http://www.beautiful-templates.com">free joomla template</a>
	-
	<a href="http://www.beautiful-templates.com">free wordpress theme</a>
</div>
<?php 
	}
?>