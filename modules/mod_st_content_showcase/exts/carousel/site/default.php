<?php
/**
 * @copyright	beautiful-templates.com
 * @license		GNU General Public License version 2 or later;
 */

// no direct access

defined('_JEXEC') or die;

$document->addScript("modules/mod_st_content_showcase/assets/js/jquery.carouFredSel-6.2.1.js");
$document->addScript("modules/mod_st_content_showcase/assets/js/jquery.mousewheel.min.js");
$document->addScript("modules/mod_st_content_showcase/assets/js/jquery.touchSwipe.min.js");
$document->addScript("modules/mod_st_content_showcase/assets/js/jquery.transit.min.js");
$document->addScript("modules/mod_st_content_showcase/assets/js/jquery.ba-throttle-debounce.min.js");

$document->addStyleSheet("modules/mod_st_content_showcase/exts/carousel/assets/css/default.css");


if ($params->get('carousel_style', '-1') != '-1') {
	$document->addStyleSheet("modules/mod_st_content_showcase/exts/carousel/styles/".$params->get('carousel_style'));
	$jsPath = 'modules/mod_st_content_showcase/exts/carousel/styles/'.str_replace('.css', '.js', $params->get('carousel_style'));
	$js = JPATH_ROOT.'/'.$jsPath;
	if (file_exists($js)) {
		$document->addScript($jsPath);	
	}	
}

$id = 'carousel'.uniqid();

$document->addScriptDeclaration("
	jQuery.noConflict();
	(function($){
		$(document).ready(function()
		{
			var ".$id."default = {
	        	responsive: true,
	        	prev: {
	        		button: '#".$id." .nav .prev'
	        	},
	        	next: {
	        		button: '#".$id." .nav .next'
	        	},
				width: '".$params->get('carousel_width', '100%')."',
				height: '".$params->get('carousel_height', '271px')."',
				auto: ".$params->get('carousel_auto', 'false').",
				scroll: {
					items: ".$params->get('carousel_next_items', 1).",
					fx: '".$params->get('carousel_fx', 'scroll')."',
					easing: '".$params->get('carousel_easing', 'swing')."',
					pauseOnHover: ".$params->get('carousel_pauseOnHover', 'false').",
					duration: ".$params->get('carousel_scroll_duration', 500)."
				},
				items: {
					width: 220,
					height: 'auto',
					visible: {
						min: 1,
						max: ".$params->get('carousel_visible_items', '4').",
					}
				},
				swipe: {
					onTouch: ".$params->get('carousel_swipe', 'true').",
					onMouse: ".$params->get('carousel_swipe', 'true')."
				},
				mousewheel: ".$params->get('carousel_mousewheel', 'false')."
			}
			
			$('#".$id." > .contain').carouFredSel(".$id."default);
		});		
	})(jQuery);
");
?>


<div class="st-content-showcase-carousel <?php echo $params->get('carousel_nav', 'nav-bottom');  ?> <?php echo str_replace('.css', '', $params->get('carousel_style', '')); ?>"  
		id="<?php echo $id; ?>" style="width:<?php echo $params->get('carousel_width', '100%'); ?>; height:<?php echo $params->get('carousel_height', '270px'); ?>">
	<div class="contain clearfix">
	<?php foreach ($list as $item) :?>
		<div class="item">
			<div class="inner">
				<?php require ST_DIR_LAYOUTS.'media.php'; ?>
				
				<div class="info <?php echo $classImage; ?>">
					
					<?php require ST_DIR_LAYOUTS.'title.php'; ?>
					
					<?php require ST_DIR_LAYOUTS.'category.php'; ?>
					
					<?php require ST_DIR_LAYOUTS.'introtext.php'; ?>
				</div>
				
				<?php if( $params->get('source') == 'hikashop' && $params->get('hikashop_show_addtocart') == '1' ):?>
					<?php echo $item->addtocart; ?>
				<?php endif;?>
				
				<?php if( $params->get('source') == 'hikashop' && $params->get('hikashop_show_price') == '1' ):?>
					<div class="price">
						<?php echo $item->price; ?>
					</div>
				<?php endif;?>
			</div>
		</div>
	<?php endforeach; ?>
	</div>
	<?php if ($params->get('carousel_nav', 'nav-bottom') != '1')  {?>
	<div class="nav clearfix">
		<div class="prev"><?php echo $params->get('carousel_prev', '<i class="icon-angle-left icon-2x"></i>'); ?></div>
		<div class="next"><?php echo $params->get('carousel_next', '<i class="icon-angle-right icon-2x"></i>'); ?></div>
	</div>
	<?php } ?>
</div>
