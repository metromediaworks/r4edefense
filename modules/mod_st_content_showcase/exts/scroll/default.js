(function($){
	$(document).ready(function(){
		function checkInview(index, ele, view, dir) 
		{
			if (!dir) // vertical 
			{
				var eleTop = $(ele).position().top;
				var view = $(view);
				
				eleBottom = eleTop + $(ele).height();
				viewTop = $(view).css('top').replace('px', '');
				viewH = $(view).parent().height();
				viewBottom = Number(viewTop) + -viewH;
				
				return (viewTop - 100 > -eleBottom) && (viewBottom < -eleTop-30 );	
			} 
			else // horizontal 
			{
				var eleLeft = $(ele).position().left;
				var view = $(view);
				
				eleRight = eleLeft + $(ele).width();
				viewLeft = $(view).css('left').replace('px', '');
				viewW = $(view).parent().width();
				viewRight = Number(viewLeft) + -viewW;
				
				return (viewLeft - 30 > -eleRight) && (viewRight < -eleLeft - 30 );	
			}
					
		}
		
		$('.st-content-showcase-scroll > .contain').each(function(){
			var contain = this;
			var horizontal = $(contain).parent().hasClass('horizontal');
			
			$(this).mCustomScrollbar({
				scrollButtons:{
					enable:true
				},
				horizontalScroll: horizontal ? true : false,
				callbacks:{
					whileScrolling: function(){
						var dir = $(this).find('.mCSB_horizontal').length;
						var view = $(this).find('.mCSB_container');
						
						$(view).find('.item').each(function(index, el) {
							if (checkInview(index, this, view, dir)) {
								$(this).addClass('in-view').removeClass('out-view');
							} else {
								$(this).removeClass('in-view').addClass('out-view');
							}
						});
					} 
				}
			});
			
			var view = $(contain).find('.mCSB_container');
			
			$(view).find('.item').each(function(index, el) {
				if (checkInview(index, this, view, horizontal)) {
					$(this).addClass('in-view').removeClass('out-view');
				} else {
					$(this).removeClass('in-view').addClass('out-view');
				}
			});
		});
	});
})(jQuery);
