<?php
/**
 * @copyright	beautiful-templates.com
 * @license		GNU General Public License version 2 or later;
 */

// no direct access

defined('_JEXEC') or die;

//$document = JFactory::getDocument();
$document->addScript("modules/mod_st_content_showcase/assets/js/modernizr.custom.97074.js");
$document->addScript("modules/mod_st_content_showcase/assets/js/jquery.mousewheel.min.js");
$document->addScript("modules/mod_st_content_showcase/assets/scrollbar/jquery.mCustomScrollbar.concat.min.js");
$document->addScript("modules/mod_st_content_showcase/assets/lightbox/jquery.fancybox-1.3.4.pack.js");
$document->addScript("modules/mod_st_content_showcase/exts/scroll/default.js");

$document->addStyleSheet("modules/mod_st_content_showcase/assets/scrollbar/jquery.mCustomScrollbar.css");
$document->addStyleSheet("modules/mod_st_content_showcase/assets/lightbox/jquery.fancybox-1.3.4.css");
$document->addStyleSheet("modules/mod_st_content_showcase/exts/scroll/default.css");


if ($params->get('scroll_style', '-1') != '-1') {
	$document->addStyleSheet("modules/mod_st_content_showcase/exts/scroll/styles/".$params->get('scroll_style'));
	$jsPath = 'modules/mod_st_content_showcase/exts/scroll/styles/'.str_replace('.css', '.js', $params->get('scroll_style'));
	$js = JPATH_ROOT.'/'.$jsPath;
	if (file_exists($js)) {
		$document->addScript($jsPath);	
	}	
}

$id = uniqid();

$document->addScriptDeclaration("
	jQuery.noConflict();
	(function($){
		jQuery(document).ready(function(){
			jQuery('a[rel=".$id."]').fancybox({
				'overlayColor'		: '#000',
				'overlayOpacity'	: 0.9
			});	
		});
	})(jQuery);
");

?>
<style>
<?php echo '#a'.$id . ' > .contain {
	height: '.$params->get('scroll_height', '450px').';
}'; ?>
</style>

<div class="st-content-showcase-scroll <?php echo ($params->get('scroll_direction', 0) ? 'horizontal' : 'vertical'); ?> <?php echo str_replace('.css', '', $params->get('scroll_style', '')); ?>"  id="a<?php echo $id; ?>">
	<div class="contain default-skin">
	<?php foreach ($list as $item) :?>
		<div class="item">
			<div class="inner">
				<?php require ST_DIR_LAYOUTS.'media.php'; ?>
				
				<div class="info <?php echo $classImage; ?>">
					
					<?php require ST_DIR_LAYOUTS.'title.php'; ?>
					
					<?php require ST_DIR_LAYOUTS.'category.php'; ?>
					
					<?php require ST_DIR_LAYOUTS.'introtext.php'; ?>
				</div>
			</div>
		</div>
	<?php endforeach; ?>
	</div>
</div>
