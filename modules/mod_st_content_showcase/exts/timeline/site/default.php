<?php
/**
 * @copyright	submit-templates.com
 * @license     GNU General Public License version 2 or later;
 */

// no direct access

defined('_JEXEC') or die;
//var_dump($list);
$local_url = JUri::base(true);
$source = $params->get('source');
$offset = $params->get('timeline_load_offset', 0); 
$id = uniqid();
?>
<div class="st-timeline-showcase"  id="<?php echo $id; ?>">
	<div class="timeline">
		<?php 
			$pages = array();
			ob_start(); 
		?>
		<?php foreach ($list as $k => $item) :?>
			<?php if($k%2==0): ?>
			<div class="row-fluid left">
				<div class="item span6">
					<div class="inner-item">
						<?php require ST_DIR_LAYOUTS.'image.php'; ?>
						
						<div class="content">
							<?php require ST_DIR_LAYOUTS.'title.php'; ?>
							
							<?php require ST_DIR_LAYOUTS.'introtext.php'; ?>
							
							<dl class="article-info">
							<?php 
								if (isset($item->created) && $params->get('timeline_date')) {
									echo '<dd class="date">' . JHTML::_('date', $item->created, JText::_('DATE_FORMAT_LC3')) . '</dd>';
								}
							?>
							
							<?php 
								if ($params->get('timeline_author') && isset($item->author)){
									echo "<dd class='author'><span>by </span>".$item->author."</dd>";
								} 
							?>
							</dl>
						</div>
					</div>
				</div>
				<div class="span6">
					<?php 
						if (isset($item->created) && $params->get('timeline_date')) {
							$time = strtotime($item->created);
							$year = date("Y",$time);
							echo "<p class='year'>".$year."</p>";
						}
					?>
				</div>
			</div>
			<?php else: ?>
			<div class="row-fluid right">
				<div class="span6">
					<?php 
						if (isset($item->created) && $params->get('timeline_date')) {
							$time = strtotime($item->created);
							$year = date("Y",$time);
							echo "<p class='year'>".$year."</p>";
						}
					?>
				</div>
				<div class="item span6">
					<div class="inner-item">
						<?php require ST_DIR_LAYOUTS.'image.php'; ?>
						
						<div class="content">
							<?php require ST_DIR_LAYOUTS.'title.php'; ?>
							
							<?php require ST_DIR_LAYOUTS.'introtext.php'; ?>
							
							<dl class="article-info">
							<?php 
								if (isset($item->created) && $params->get('timeline_date')) {
									echo '<dd class="date">' . JHTML::_('date', $item->created, JText::_('DATE_FORMAT_LC3')) . '</dd>';
								}
							?>
							
							<?php 
								if ($params->get('timeline_author') && isset($item->author)){
									echo "<dd class='author'><span>by </span>".$item->author."</dd>";
								} 
							?>
							</dl>
						</div>
					</div>
				</div>
			</div>
			<?php endif;?>
		
		<?php 
			if ($offset > 0 ) {
				$page = ($k+1) % $offset;
				if ($page < 1) {
					echo '[ST-CONTENT-SHOWCASE-LOAD-MORE]';
				}
			} 
		?>
		
		<?php endforeach; ?>
		<?php 
			$pages = ob_get_contents();
			ob_end_clean();
			$pages = explode('[ST-CONTENT-SHOWCASE-LOAD-MORE]', $pages);
			if (count($pages) > 0) {
				echo $pages[0];
			} 
		?>
	</div>
	<?php if (count($pages) > 1) {
		echo '<div class="st-content-showcase-load-more">Load More</div>';
	}
	?>
</div>
<?php 
$document->addScriptDeclaration("
	jQuery.noConflict();
	(function($){
		
		$(document).ready(function(){
			var contain = $('#".$id."').find('.timeline');								
			
			html = contain.html();	

														
			// load more
			stShowcaseDataLoadMore".$id." = ".json_encode($pages).";
			
			new_item = $('');
			stShowcaseDataLoaMoreCount".$id." = 1;
			$(document).on('click', '#".$id." .st-content-showcase-load-more:not(end-load-more)', function(index){
				if (stShowcaseDataLoadMore".$id."[stShowcaseDataLoaMoreCount".$id."]) {
					newItems = $(stShowcaseDataLoadMore".$id."[stShowcaseDataLoaMoreCount".$id."]).addClass('new_item');
					
					var article = 'article';
					if(".$source."==='article'){
						img = newItems.find('.image').find('img');
						img.each(function(){
							//console.log($(this).attr('src'));
							old_url = $(this).attr('src');
							new_url = '".$local_url."'+'/'+old_url;
							//console.log(new_url);
							$(this).attr('src',new_url);
						})
					};
					//console.log(img.attr('src'));
					//newItems.css({'opacity':'0','height':'0'});
					contain.append(newItems);
					// newItems.animate({
						// opacity: 1,
						// height: 'auto'
					// },800);
					
					stShowcaseDataLoaMoreCount".$id."++;
					new_item = new_item.add(newItems);
				};
				if (stShowcaseDataLoadMore".$id."[stShowcaseDataLoaMoreCount".$id."]){
					
				}
				else {
					$(this).addClass('end-load-more');
				}
				return;
			});
			
			$(document).on('click', '#".$id." .st-content-showcase-load-more.end-load-more', function(event){
				new_item.animate({
					opacity: 0,
					height: 0
				},500, function(){
					$(this).remove();
				});
				
				$('#".$id." .end-load-more').removeClass('end-load-more');
				stShowcaseDataLoaMoreCount".$id." = 1;
				new_item= $('');
				return;
			});
			
			
		});	
	})(jQuery);
");
?>