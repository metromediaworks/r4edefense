<?php
/**
 * @copyright	beautiful-templates.com
 * @license		GNU General Public License version 2 or later;
 */

// no direct access

defined('_JEXEC') or die;
$local_url = JUri::base(true);
$source = $params->get('source');
$document->addScript("modules/mod_st_content_showcase/assets/js/modernizr.custom.97074.js");
$document->addScript("modules/mod_st_content_showcase/exts/wall/isotope.js");
$document->addScript("modules/mod_st_content_showcase/exts/wall/load-images.js");
$document->addScript("modules/mod_st_content_showcase/exts/wall/resize.js");
$document->addScript("modules/mod_st_content_showcase/assets/js/jquery.mousewheel.min.js");
$document->addScript("modules/mod_st_content_showcase/assets/lightbox/jquery.fancybox-1.3.4.pack.js");

$document->addStyleSheet("modules/mod_st_content_showcase/exts/wall/layout.css");
$document->addStyleSheet("modules/mod_st_content_showcase/assets/lightbox/jquery.fancybox-1.3.4.css");

if ($params->get('wall_style', '-1') != '-1') {
	$document->addStyleSheet("modules/mod_st_content_showcase/exts/wall/styles/".$params->get('wall_style'));
	$jsPath = 'modules/mod_st_content_showcase/exts/wall/styles/'.str_replace('.css', '.js', $params->get('wall_style'));
	$js = JPATH_ROOT.'/'.$jsPath;
	if (file_exists($js)) {
		$document->addScript($jsPath);	
	}	
}

$id = uniqid();
?>
<div class="st-content-showcase-wall <?php echo str_replace('.css', '', $params->get('wall_style', '')); ?>"  id="<?php echo $id; ?>">
	<?php if ($params->get('wall_category') && count($categories)) :?>
		<div class="category-wall">
				<span data-filter="all"><?php echo JText::_('ST_WALL_CATEGORY_ALL') ?></span>
			<?php foreach ($categories as $cate): ?>
				<?php $cate = ucfirst(basename($cate)); ?>
				<span data-filter="<?php echo preg_replace('/[^a-zA-Z]/', 'a', strtolower($cate)); ?>"><?php echo $cate; ?></span>
			<?php endforeach; ?>
		</div>
	<?php endif; ?>
	<?php $offset = $params->get('wall_load_offset', 0); ?>
	<div class="layout-wall">
		<?php 
			$pages = array();
			ob_start(); 
		?>
		<?php foreach ($list as $index => $item) :?>
			
			<div class="item <?php echo (isset($item->category) ? preg_replace('/[^a-zA-Z]/', 'a', basename(strtolower($item->category))) : ''); ?>">
				<div class="inner">
					<?php require ST_DIR_LAYOUTS.'media.php'; ?>
					
					<div class="info <?php echo $classImage; ?>">
						
						<?php require ST_DIR_LAYOUTS.'title.php'; ?>
						
						<?php require ST_DIR_LAYOUTS.'category.php'; ?>
						
						<?php require ST_DIR_LAYOUTS.'introtext.php'; ?>
					</div>
				</div>
			</div>
			<?php 
				if ($offset > 0 ) {
					$page = ($index+1) % $offset;
					if ($page < 1) {
						echo '[ST-CONTENT-SHOWCASE-LOAD-MORE]';
					}
				} 
			?>
		<?php endforeach; ?>
		<?php 
			$pages = ob_get_contents();
			ob_end_clean();
			$pages = explode('[ST-CONTENT-SHOWCASE-LOAD-MORE]', $pages);
			if (count($pages) > 0) {
				echo $pages[0];
			} 
		?>
	</div>
	<?php if (count($pages) > 1) {
		echo '<div class="st-content-showcase-load-more">Load More</a>';
	}
		
	?>
</div>
<?php 
$document->addScriptDeclaration("
	jQuery.noConflict();
	(function($){
		var wallParams = ".$params->toString().";
		setCols = function (contain, containerWidth, cols) 
		{
			var widthWindow = $(window).width();
			
			if (widthWindow <= 320) {
				cols = wallParams.wall_grid_cols_320;
			} else if (widthWindow <= 480) {
				cols = wallParams.wall_grid_cols_480;
			} else if (widthWindow <= 768) {
				cols = wallParams.wall_grid_cols_767;
			}
			
			if (widthWindow <= 320) {
				itemWidth = containerWidth/wallParams.wall_grid_cols_320;
			} else if (widthWindow <= 480) {
				itemWidth = containerWidth/wallParams.wall_grid_cols_480;
			} else if (widthWindow <= 768){
				itemWidth = containerWidth/wallParams.wall_grid_cols_767;
			} else {
				itemWidth = containerWidth/cols - 0.1;
			}
			
			$(contain + ' .item').each(function(el)
			{
				if (wallParams.wall_item_width == 'auto') {
					width = $(this).width();
					offset = Math.round(width/itemWidth);
					$(this).css('width', itemWidth * offset);	
				} else {
					$(this).css('width', itemWidth);
				}
			});
			
			return cols;	
		}
		
		$(document).ready(function(){
			var wrapper = $('#".$id."');
			var contain = wrapper.find('.layout-wall');
			
			contain.imagesLoaded(function()
			{
				var cols = setCols('#".$id."', contain.width(), wallParams.wall_grid_cols);
				var options = {};
				if (wallParams.wall_fit_row != '0') {
					options.layoutMode = 'fitRows';
				}
				
				contain.isotope(options);
				
				// update columnWidth on window resize
				$(window).on('throttledresize', function( event ) {
					cols = setCols('#".$id."', contain.width(), wallParams.wall_grid_cols);
				});
				
				$('a[rel=".$id."]').fancybox({
					'titlePosition'		: 'outside',
					'overlayColor'		: '#000',
					'overlayOpacity'	: 0.9,
					'titlePosition' 	: 'over'
				});
			});
			
			// category
			wrapper.find('.category-wall span[data-filter=\"all\"]').addClass('current');
			wrapper.find('.category-wall span').each(function()
			{
				$(this).click(function()
				{	
					var className = $(this).attr('data-filter');
					if (className == 'all') {
						className = 'item';
					}
					
					contain.isotope({ filter: '.' + className, transitionDuration : '0.8s'});
					wrapper.find('.category-wall span').removeClass('current');
					$(this).addClass('current');
				});
			});
			
			// load more
			stShowcaseDataLoadMore".$id." = ".json_encode($pages).";
			stShowcaseDataLoaMoreCount".$id." = 1;
			$('#".$id." .st-content-showcase-load-more').click(function(index) 
			{
				if (stShowcaseDataLoadMore".$id."[stShowcaseDataLoaMoreCount".$id."]) {
					newItems = $(stShowcaseDataLoadMore".$id."[stShowcaseDataLoaMoreCount".$id."]);
					var article = 'article';
					if(".$source."==='article'){
						img = newItems.find('.media').find('img');
						img.each(function(){
							//console.log($(this).attr('src'));
							old_url = $(this).attr('src');
							new_url = '".$local_url."'+'/'+old_url;
							//console.log(new_url);
							$(this).attr('src',new_url);
						})
					};
					contain.append(newItems);
					contain.imagesLoaded(function(){
						cols = setCols('#".$id."', contain.width(), wallParams.wall_grid_cols);
						contain.isotope('appended', newItems);	
					});
					
					//contain.isotope( 'reloadItems' ).isotope({ sortBy: 'original-order' });
					//contain.isotope( 'reLayout' );
					$('a[rel=".$id."]').fancybox({
						'titlePosition'		: 'outside',
						'overlayColor'		: '#000',
						'overlayOpacity'	: 0.9,
						'titlePosition' 	: 'over'
					});
					stShowcaseDataLoaMoreCount".$id."++;
				} else {
					$(this).addClass('end-load-more');
				}
			})
		});	
	})(jQuery);
");
?>