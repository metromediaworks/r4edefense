<?php
/**
 * @package     Joomla.Legacy
 * @subpackage  Form
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE
 */

defined('JPATH_PLATFORM') or die;

JFormHelper::loadFieldClass('list');

/**
 * Form Field class for the Joomla Platform.
 * Supports an HTML select list of categories
 *
 * @package     Joomla.Legacy
 * @subpackage  Form
 * @since       11.1
 */
class JFormFieldHikashopCategory extends JFormFieldList
{
	/**
	 * The form field type.
	 *
	 * @var    string
	 * @since  11.1
	 */
	public $type = 'HikashopCategory';

	/**
	 * Method to get the field options for category
	 * Use the extension attribute in a form to specify the.specific extension for
	 * which categories should be displayed.
	 * Use the show_root attribute to specify whether to show the global category root in the list.
	 *
	 * @return  array    The field option objects.
	 *
	 * @since   11.1
	 */
	protected function getOptions()
	{
		$options = array();
		
		$db = &JFactory::getDBO();
		
		$query = 'SELECT category_id, category_name'
	   . ' FROM #__hikashop_category'
	   . ' WHERE category_type = "product" '
	   . ' AND category_published = "1" '
	   . ' ORDER BY category_id';
	   $db->setQuery( $query );
	   $result = $db->loadObjectList();
	   //var_dump($result);
		
		foreach($result as $key=>$value){
			$options[]= JHtml::_('select.option', $value->category_id, JText::_($value->category_name));
		}

		return $options;
	}
	
	
}
