<?php

defined('_JEXEC') or die( 'Restricted access' );

class JFormFieldloadCSSJSAdmin extends JFormField
{
    protected $type = 'loadCSSJSAdmin';

	protected function getInput()
	{
		require_once dirname(dirname(__FILE__)).'/defined.php';
		$doc = JFactory::getDocument();
		if (version_compare(JVERSION, '3.0.0','<')) {
			$doc->addScript('http://code.jquery.com/jquery-latest.min.js');	
			$doc->addScriptDeclaration("$.noConflict();");
		}
		$doc->addStyleSheet(JURI::root().'modules/'.ST_NAME.'/assets/css/admin.css');
		$doc->addScript(JURI::root().'modules/'.ST_NAME.'/assets/js/admin.js');
		
		return '';
	}

	/**
	 * Method to get the field label markup.
	 *
	 * @return  string  The field label markup.
	 * @since   11.1
	 */
	protected function getLabel()
	{
		return '';
	}
}

?>