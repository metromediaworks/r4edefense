<?php

defined('_JEXEC') or die( 'Restricted access' );
require_once JPATH_ROOT.'/libraries/joomla/form/fields/folderlist.php';
class JFormFieldstfolderlist extends JFormFieldFolderList
{
    public $type = 'stfolderlist';

	/**
	 * Method to get the field options.
	 *
	 * @return  array  The field option objects.
	 *
	 * @since   11.1
	 */
	protected function getOptions()
	{
		$options = array();
		
		$path = (string) $this->element['directory'];

		if (!is_dir($path))
		{
			$path = JPATH_ROOT . '/' . $path;
		}
		
		// Get a list of folders in the search path with the given filter.
		$folders = JFolder::folders($path, $this->filter);

		// Build the options list from the list of folders.
		if (is_array($folders))
		{
			foreach ($folders as $folder)
			{
				// Check to see if the file is in the exclude mask.
				if ($this->exclude)
				{
					if (preg_match(chr(1) . $this->exclude . chr(1), $folder))
					{
						continue;
					}
				}

				$options[] = JHtml::_('select.option', $folder, $folder);
			}
		}
		//var_dump($folders);
		return $options;
	}
}

?>