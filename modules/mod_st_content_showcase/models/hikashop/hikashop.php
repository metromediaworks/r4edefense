<?php
/**
 * @copyright	submit-templates.com
 * @license		GNU General Public License version 2 or later;
 */

// no direct access
defined('_JEXEC') or die;
if(!include_once(rtrim(JPATH_ADMINISTRATOR,DS).DS.'components'.DS.'com_hikashop'.DS.'helpers'.DS.'helper.php')){
	echo 'This module can not work without the Hikashop Component';
	return;
}

class stContentShowcaseModelHikashop extends stContentShowcaseModel {
	function _getCheckoutURL(){
		global $Itemid;
		$url_itemid='';
		if(!empty($Itemid)){
			$url_itemid='&Itemid='.$Itemid;
		}
		return hikashop_completeLink('checkout'.$url_itemid,false,true);
	}
	function init($cart=false){
		$config =& hikashop_config();
		$url = $config->get('redirect_url_after_add_cart','stay_if_cart');
		switch($url){
			case 'checkout':
				$url = $this->_getCheckoutURL();
				break;
			case 'stay_if_cart':
				$url='';
				if(!$cart){
					$url = $this->_getCheckoutURL();
					break;
				}
			case 'ask_user':
			case 'stay':
				$url='';
			case '':
			default:
				if(empty($url)){
					$url = hikashop_currentURL('return_url');
				}
				break;
		}

		return urlencode($url);
	}
	
	public function  getCategories () 
	{
		//var_dump($this->_params->get('hikashop_category', array()));
		$categories = $this->_params->get('hikashop_category', array());	
			
		$db	= JFactory::getDBO();
		$query = 'SELECT category_id, category_name FROM '.hikashop_table('category').' WHERE category_id IN ('.implode(',',$categories).') ORDER BY category_id';
		$db->setQuery($query);
		$results = $db->loadObjectList();
		$items = array();
		foreach ($results as $result) {
			$items[$result->category_id] = $result->category_name;
		}
		return $items;
	}
	
	public function _items() 
	{
		$app = JFactory::getApplication();
		$db	= JFactory::getDBO();
		$config =& hikashop_config();
		//$default_params =  $config->get('default_params');
		$image_options = array('default' => true,'forcesize'=>$config->get('image_force_size',true),'scale'=>$config->get('image_scale_mode','inside'));
		
		//Get Menu Item
		global $Itemid;
		$menus	= $app->getMenu();
		$menu	= $menus->getActive();
		if(empty($menu)){
			if(!empty($Itemid)){
				$menus->setActive($Itemid);
				$menu	= $menus->getItem($Itemid);
			}
		}
		$url_itemid = '';
		if(!empty($Itemid)){
			$url_itemid = '&Itemid='.(int)$Itemid;
		}

		if ($this->_params->get('item_id', '') != '') {
			$url_itemid .= '&Itemid='.$this->_params->get('item_id', '');
		}
		
		$categories = $this->_params->get('hikashop_category', array());	
		
		//Show Childs of Categories
		if($this->_params->get('hikashop_show_childrens')){
			$categoryClass = hikashop_get('class.category');
			$categoryClass->type = 'product';
			$childs = $categoryClass->getChilds($categories,true,array(),'',0,0);
			foreach($childs as $children){
				$categories[] = $children->category_id;
			}
		}	
		
		// Query get product id from categories selected.
		$query1 = 'SELECT pr_cat.product_id FROM #__hikashop_category AS cat RIGHT JOIN #__hikashop_product_category AS pr_cat ON cat.category_id = pr_cat.category_id 
		WHERE cat.category_id IN ('.implode(',',$categories).')  ';
		$db->setQuery($query1);
		$results = $db->loadObjectList();
		
		// Create array get product with filter 
		$product_array = array();
		foreach ($results as $result) {
			$product_array[] = $result->product_id;
		}
		
		$ordering = $this->_params->get('hikashop_ordering',1);
		switch ($ordering) {
			case '1':
				$orderby = 'product_id ASC';
				break;
			case '2':
				$orderby = 'product_id DESC';
				break;
			case '3':
				$orderby = 'product_name ASC';
				break;
			case '4':
				$orderby = 'product_name DESC';
				break;
			case '5':
				$orderby = 'product_created ASC';
				break;
			case '6':
				$orderby = 'product_created DESC';
				break;
			case '7':
				$orderby = 'RAND()';
				break;
			case '8':
				$orderby = 'product_sales ASC';
				break;
			case '9':
				$orderby = 'product_sales DESC';
				break;
			case '10':
				$orderby = 'product_hit ASC';
				break;
			case '11':
				$orderby = 'product_hit ASC';
				break;
			default:
				$orderby = 'product_id ASC';
				break;
		}
		
		// Query get product infor from database 
		$query2 = 'SELECT product_id, product_name, product_alias, product_description, product_msrp  FROM #__hikashop_product  
		WHERE product_published = "1" AND product_id IN ('.implode(',',$product_array).') ORDER BY '.$orderby.'';
		$db->setQuery($query2);
		$products = $db->loadObjectList();
		
		//Create array items 
		$items = array();
		foreach ($products as $product) {
			
			if (count($items) > $this->_params->get('count', 5)) {
				break;
			}
			
			$item = new stdClass;
			$item->id = $product->product_id;
			$item->title = $product->product_name;
			$item->alias = $product->product_alias;
			$item->introtext = $product->product_description;
			
			//Set Category
			$cat = hikashop_get('class.product');
			$cat_id = $cat ->getCategories($item->id);
			$query_cat = 'SELECT category_name FROM '.hikashop_table('category').' WHERE category_id = '.$cat_id[0].' ';
			$db->setQuery($query_cat);
			$cat_name = $db->loadObjectList();
			
			$item->category = ucfirst($cat_name[0]->category_name);
			
			//Set image for product
			$query_file = 'SELECT file_path ,file_name FROM '.hikashop_table('file').' WHERE file_ref_id = '.$product->product_id.' AND file_type=\'product\' ORDER BY file_ref_id ASC, file_ordering ASC, file_id ASC';
			$db->setQuery($query_file);
			$file = $db->loadObjectList();
			$image = hikashop_get('helper.image');
			$img = $image->getThumbnail($file[0]->file_path, array('width' => $image->main_thumbnail_x, 'height' => $image->main_thumbnail_y), $image_options);
			
			$item->image_intro = $img->url;
			$item->image_large = $img->url;
			$item->image_intro_caption = $file[0]->file_name;
			$item->image_intro_alt = $file[0]->file_name;
			
			//Set link
			$item->link =  hikashop_completeLink('product&task=show&cid='.$item->id.'&name='.$item->alias.$url_itemid);
			
			//Set Add to cart button
			$classjs = hikashop_get('helper.cart');
			$classjs->cartCount(true);
			$url = $this->init(true);
			$classjs->getJS($url);
			$ajax = 'return hikashopModifyQuantity(\''.$item->id.'\',field,1,0,\'cart\',0)';
			
			$item->addtocart = '<form action="'.hikashop_completeLink('product&task=updatecart').'" method="post" name="hikashop_product_form_'.$item->id.'" enctype="multipart/form-data">';
			$item->addtocart .= $classjs->displayButton(JText::_('ADD_TO_CART'),'add',$this->_params,'',$ajax,'','0','1');
			$item->addtocart .=	'<input type="hidden" name="hikashop_cart_type_"'.$item->id.'" id="hikashop_cart_type_"'.$item->id.'" value="cart"/>';
			$item->addtocart .=	'<input type="hidden" name="product_id" value="'.$item->id.'" />';
			$item->addtocart .=	'<input type="hidden" name="module_id" value="" />';
			$item->addtocart .=	'<input type="hidden" name="add" value="1"/>';
			$item->addtocart .=	'<input type="hidden" name="ctrl" value="product"/>';
			$item->addtocart .=	'<input type="hidden" name="task" value="updatecart"/>';
			$item->addtocart .=	'<input type="hidden" name="return_url" value="'.urlencode(base64_encode(urldecode($url))).'"/>';
			$item->addtocart .= '</form>';
			
			//Set Price for product
			$currencyHelper = hikashop_get('class.currency');
			$mainCurr = $currencyHelper->mainCurrency();
			
			$query_price = 'SELECT price_value, price_currency_id FROM '.hikashop_table('price').' WHERE price_product_id = '.$product->product_id.' AND price_currency_id='.$mainCurr.' ORDER BY price_value DESC ';
			$db->setQuery($query_price);
			$price = $db->loadObjectList();
			$item->price = $currencyHelper->format($price[0]->price_value,$price[0]->price_currency_id);
				
			
			$items[]= $item;
		}
		return $items;
	}
}
