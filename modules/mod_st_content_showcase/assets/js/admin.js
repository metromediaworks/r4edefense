jQuery( document ).ready(function( $ ) {
	setTimeout(function(){
		
		
		//2.5
		panels = $('#module-sliders > div');
		
		if (panels.length > 0 ) 
		{
			panels.each(function(){
			panel = $(this);
			id = panel.find('h3').attr('id');
			if (id) {
					
					if (id.match(/source_|showcase_/)) {
						panel.css('display', 'none');
					}
				}
			});	
		
			selectShowcase = $('#jform_params_extension');
			selected = selectShowcase.find(':selected').val();
			
			panels.each(function(){
				panel = $(this);
				id = panel.find('h3').attr('id');
				if (id) {
					if (id == 'showcase_'+ selected + '-options') {
						panel.css('display', 'block');
					}
				}
			});
			
			
			
			selectShowcase.change(function(){
				val = $(this).find(':selected').val();
				
				panels.each(function(){
					panel = $(this);
					id = panel.find('h3').attr('id');
					if (id) {
						if (id.match(/showcase_/)) {
							panel.css('display', 'none');
						}
						
						if (id == 'showcase_'+ val + '-options') {
							panel.css('display', 'block');
						}
					}
				});
			});
			
			selectSource = $('#jform_params_source');
			selected = selectSource.find(':selected').val();
			
			panels.each(function(){
				panel = $(this);
				id = panel.find('h3').attr('id');
				if (id) {
					if (id == 'source_'+ selected + '-options') {
						panel.css('display', 'block');
					}
				}
			});
			
			selectSource.change(function(){
				val = $(this).find(':selected').val();
				
				panels.each(function(){
					panel = $(this);
					id = panel.find('h3').attr('id');
					
					if (id) {
						if (id.match(/source_/)) {
							panel.css('display', 'none');
						}
						
						if (id == 'source_'+ val + '-options') {
							panel.css('display', 'block');
						}
					}
				});
			});
		}
		
		
		
		// 3.0
		tabs = $('#myTabTabs a'); 
		
		if (tabs.length > 0) 
		{
			tabs.each(function(){
				tab = $(this);
				if(tab.attr('href').match(/#attrib-source_|#attrib-showcase_/)) {
					tab.css('display', 'none');
				}
				//console.log(this.href.include('$attrib-source_'));	
			});
			
			selectShowcase = $('#jform_params_extension');
			selected = selectShowcase.find(':selected').val();
			tabs.filter('[href=#attrib-showcase_'+ selected +']').css('display', 'block');
			
			selectShowcase.change(function(){
				val = $(this).find(':selected').val();
				tabs.filter('[href*=#attrib-showcase_]').css('display', 'none');
				tabs.filter('[href=#attrib-showcase_'+ val +']').css('display', 'block');
			});
			
			selectSource = $('#jform_params_source');
			selected = selectSource.find(':selected').val();
			tabs.filter('[href=#attrib-source_'+ selected +']').css('display', 'block');
			
			selectSource.change(function(){
				val = $(this).find(':selected').val();
				tabs.filter('[href*=#attrib-source_]').css('display', 'none');
				tabs.filter('[href=#attrib-source_'+ val +']').css('display', 'block');
			});
		}
		
	}, 1000);
});
	

