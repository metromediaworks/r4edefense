<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_st_contact
 *
 * @copyright   Copyright (C) 2014 by Anh Minh - Beautiful-templates.com
 * @license     
 */

defined('_JEXEC') or die;

/**
 * Helper for mod_st_contact
 *
 * @package     Joomla.Site
 * @subpackage  mod_st_contact
 * @since       2.5
 */
function  test_input($data)
{
   //$data = trim($data);
   $data = stripslashes($data);
   $data = htmlspecialchars($data);
   return $data;
}
class ModStContactHelper
{
	
	
	public static function getAjax()
	{
		jimport('joomla.application.module.helper');
		$input  		= JFactory::getApplication()->input;
		$module 		= JModuleHelper::getModule('st_contact');
		$params 		= new JRegistry();
		$params->loadString($module->params);
		
		//load from params administrator
		$recipient = $params->get('email_recipient','contact@gmail.com');
		$name_from = $params->get('name_from','Admin Beautiful Templates');
		$email_from = $params->get('email_from','beautiful-templates@gmail.com');
		
		//get value from input
		$name  = $input->get('name');
		$email  = $input->get('email');
		$subject  = $input->get('subject');
		$messages  = $input->get('messages');
		
		if( $name == '' || $email == '' || $subject == '' || $messages == ''){
			$output = json_encode(
				array(
					'type'=>'error', 
					'text' => 'Please fill in field empty'
				)
			);
			die($output);
		}else{
			$name = test_input($name);
			$subject = test_input($subject);
			$messages = test_input($messages);
			if((!preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/", strtolower($_POST["email"])))){
				$output = json_encode(
					array(
						'type'=>'error', 
						'text' => 'Please fill in valid email !'
					)
				);
				die($output);
			}else{
				$email = test_input($email);
			
				$cc=null;
				$bcc=null;
				$attachment =null;
				$mail = & JFactory::getMailer();
				
				$body = "You received a message from ". $email ." with massages :"."\n\n". $messages;
				
				$subject= 'Hello '.$recipient.' ';
				$subject.= 'You received a email from '.$name.' ';
				$subject.= 'with subject : '.$subject.'';
		
		        $mail->setSubject($subject);
		        $mail->setBody($body);
				
				// Are we sending the email as HTML?
		       
		        $mail->IsHTML(true);
		        
				$mail->addRecipient($recipient);
				$mail->setSender(array($email_from, $name_from));
		        $mail->addCC($cc);
		        $mail->addBCC($bcc);
		        $mail->addAttachment($attachment);
				
				$mail->addReplyTo($email,$name);
		 
				if(!$mail->Send()) {
					$output = json_encode(
						array(
							'type'=>'error', 
							'text' => 'Send mail fail !'. $mail->ErrorInfo
						)
					);	
					die($output);
				} else {
					$output = json_encode(
						array(
							'type'=>'success', 
							'text' => 'Thank you for your contact !'
						)
					);
					die($output);
				}
			}
		};

	}
}
