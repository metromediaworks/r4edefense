<?php
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
$document = JFactory::getDocument();
$document->addStyleSheet(JURI::base() . 'modules/mod_st_contact/tmpl/default.css');
$document->addScriptDeclaration("
	jQuery.noConflict();
	(function($){		
		$(document).ready(function(){
			$('.st-contact-form input.submit').click(function(event){
				event.preventDefault();
				//get input field values
		        var name       = $('.st-contact-form input[name=name]').val(); 
		        var email      = $('.st-contact-form input[name=email]').val();
		        var subject    = $('.st-contact-form input[name=subject]').val();
		        var messages   = $('.st-contact-form textarea[name=messages]').val();
		        
		        //Check input empty
		        var proceed = true;
		        var alert = '';
		        if(name==''){ 
		            alert += '<p class=\"error\">You must enter your name !</p>';
		            proceed = false;
		        }
		        if(email==''){ 
		            alert += '<p class=\"error\">You must enter your email !</p>';
		            proceed = false;
		        }
		        if(subject=='') {    
		            alert += '<p class=\"error\">You must enter subject !</p>';
		            proceed = false;
		        }
		        if(messages=='') {  
		            alert += '<p class=\"error\">You must enter messages !</p>';
		            proceed = false;
		        }
				
				if(proceed){
					request = {
							'option' 	: 'com_ajax',
							'module' 	: 'st_contact',
							'format' 	: 'json',
							'name'	 	:  name,
							'email'  	:  email,
							'subject'	:  subject,
							'messages'	:  messages	
						};
					$.ajax({
						type   : 'POST',
						data   : request,
						dataType: 'json',
						success: function (response) {
							if(response.type == 'error')
							{
							  	output = '<p class=\"error\">'+response.text+'</p>';
							}else{
								output = '<p class=\"success\">'+response.text+'</p>';
									
								//reset values in all input fields
							  	$('.st-contact-form input[type=text]').val(''); 
								$('.st-contact-form textarea').val(''); 
							}
							$('.st-contact-form #show-messages').hide().html(output).slideDown(700);
							
						}
					});
				}else{
					$('.st-contact-form #show-messages').hide().html(alert).slideDown(700);
				}
				setTimeout(function(){
					$('.st-contact-form #show-messages').slideUp(700);
				},4000);
			});
		});	
	})(jQuery);
");
require_once __DIR__ . '/helper.php';
require JModuleHelper::getLayoutPath('mod_st_contact', $params->get('layout', 'default'));