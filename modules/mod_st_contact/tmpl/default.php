<?php 

$rows_message= (int)$params->get('rows_message');
?>
<div class="st-contact-form">
	
	<?php if($params->get('show_introtext',1)):?>
	<div class="introtext">
		<?php if(($params->get('introtext') !='')):?>
		<p><?php echo htmlspecialchars($params->get('introtext')); ?></p>
		<?php endif;?>
	</div>
	<?php endif;?>
	
	<div id="show-messages"></div>
	
	<div class="row-fluid">
		
		<div class="name text">
			<?php if($params->get('show_labeltext',1)):?>
				<label><?php echo $params->get('label_name');?></label> 
			<?php endif;?>
			<input  type="text" name="name" placeholder="<?php echo $params->get('label_name','Your Name');?>" /> 
		</div>
		
		<div class="email text">
			<?php if($params->get('show_labeltext',1)):?>
				<label><?php echo $params->get('label_email');?></label>
			<?php endif;?>
			<input type="text" name="email" placeholder="<?php echo $params->get('label_email','Your Email');?>" /> 
		</div>

		<div class="subject text">
			<?php if($params->get('show_labeltext',1)):?>
				<label><?php echo $params->get('label_subject');?></label>
			<?php endif;?>
			<input  type="text" name="subject" placeholder="<?php echo $params->get('label_subject','Subject');?>" /> 
		</div>
	</div>

	<div class="message">
		<?php if($params->get('show_labeltext',1)):?>
			<label><?php echo $params->get('label_message');?></label>
		<?php endif;?>
		<textarea name="messages" placeholder="<?php echo $params->get('label_message','Message');?>" rows="<?php echo $rows_message; ?>" cols="50" ></textarea> 
	</div>
	
	<p class="submit">
		<input class="submit" type="submit" name="submit" value="<?php echo $params->get('label_submit','Submit');?>" />
	</p>
</div>