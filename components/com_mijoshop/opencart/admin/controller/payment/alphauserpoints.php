<?php
/*
* @package		MijoShop
* @copyright	2009-2013 Miwisoft LLC, miwisoft.com
* @license		GNU/GPL http://www.gnu.org/copyleft/gpl.html
* @license		GNU/GPL based on AceShop www.joomace.net
*/

// No Permission
defined('_JEXEC') or die('Restricted access');
 
class ControllerPaymentAlphauserpoints extends Controller {
	private $error = array(); 

	public function index() {
		$this->language->load('payment/alphauserpoints');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/setting');
			
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('alphauserpoints', $this->request->post);				
			
			$this->session->data['success'] = $this->language->get('text_success');

			$this->redirect($this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
		$this->data['text_all_zones'] = $this->language->get('text_all_zones');
		
		$this->data['entry_points'] = $this->language->get('entry_points');
		$this->data['entry_points_help'] = $this->language->get('entry_points_help');
		$this->data['entry_total'] = $this->language->get('entry_total');	
		$this->data['entry_order_status'] = $this->language->get('entry_order_status');		
		$this->data['entry_geo_zone'] = $this->language->get('entry_geo_zone');
		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['entry_sort_order'] = $this->language->get('entry_sort_order');
		
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

 		if (isset($this->error['points'])) {
			$this->data['error_points'] = $this->error['points'];
		} else {
			$this->data['error_points'] = '';
		}

  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_payment'),
			'href'      => $this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('payment/alphauserpoints', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
				
		$this->data['action'] = $this->url->link('payment/alphauserpoints', 'token=' . $this->session->data['token'], 'SSL');
		
		$this->data['cancel'] = $this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL');
		
		if (isset($this->request->post['alphauserpoints_points'])) {
			$this->data['alphauserpoints_points'] = $this->request->post['alphauserpoints_points'];
		} else {
			$this->data['alphauserpoints_points'] = $this->config->get('alphauserpoints_points');
		}
		
		if (isset($this->request->post['alphauserpoints_total'])) {
			$this->data['alphauserpoints_total'] = $this->request->post['alphauserpoints_total'];
		} else {
			$this->data['alphauserpoints_total'] = $this->config->get('alphauserpoints_total'); 
		} 
				
		if (isset($this->request->post['alphauserpoints_order_status_id'])) {
			$this->data['alphauserpoints_order_status_id'] = $this->request->post['alphauserpoints_order_status_id'];
		} else {
			$this->data['alphauserpoints_order_status_id'] = $this->config->get('alphauserpoints_order_status_id'); 
		} 
		
		$this->load->model('localisation/order_status');
		
		$this->data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();
		
		if (isset($this->request->post['alphauserpoints_geo_zone_id'])) {
			$this->data['alphauserpoints_geo_zone_id'] = $this->request->post['alphauserpoints_geo_zone_id'];
		} else {
			$this->data['alphauserpoints_geo_zone_id'] = $this->config->get('alphauserpoints_geo_zone_id'); 
		} 
		
		$this->load->model('localisation/geo_zone');
										
		$this->data['geo_zones'] = $this->model_localisation_geo_zone->getGeoZones();
		
		if (isset($this->request->post['alphauserpoints_status'])) {
			$this->data['alphauserpoints_status'] = $this->request->post['alphauserpoints_status'];
		} else {
			$this->data['alphauserpoints_status'] = $this->config->get('alphauserpoints_status');
		}
		
		if (isset($this->request->post['alphauserpoints_sort_order'])) {
			$this->data['alphauserpoints_sort_order'] = $this->request->post['alphauserpoints_sort_order'];
		} else {
			$this->data['alphauserpoints_sort_order'] = $this->config->get('alphauserpoints_sort_order');
		}

		$this->template = 'payment/alphauserpoints.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'payment/alphauserpoints')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if (!$this->request->post['alphauserpoints_points']) {
			$this->error['points'] = $this->language->get('error_points');
		}

		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}
}
?>