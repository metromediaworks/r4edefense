<?php
/*
* @package		MijoShop
* @copyright	2009-2013 Miwisoft LLC, miwisoft.com
* @license		GNU/GPL http://www.gnu.org/copyleft/gpl.html
* @license		GNU/GPL based on AceShop www.joomace.net
*/

// No Permission
defined('_JEXEC') or die('Restricted access');

// Heading
$_['heading_title']      	= 'JomSocial Points';

// Text 
$_['text_jomsocialpoints']	= '<img src="view/image/payment/jomsocialpoints.png" alt="JomSocial Points" title="JomSocial Points" style="border: 1px solid #EEEEEE;" />';
$_['text_payment']       	= 'Payment';
$_['text_success']       	= 'Success: You have modified cheque / money order account details!';

// Entry
$_['entry_points']     		 = 'Exchange rate:';
$_['entry_total']        	 = 'Total:<br /><span class="help">The checkout total the order must reach before this payment method becomes active.</span>';
$_['entry_points_help']      = 'The conversion rate is x Point(s) = 1 ';
$_['entry_order_status'] 	 = 'Order Status:';
$_['entry_geo_zone']     	 = 'Geo Zone:';
$_['entry_status']       	 = 'Status:';
$_['entry_sort_order']   	 = 'Sort Order:';

// Error
$_['error_permission']   	 = 'Warning: You do not have permission to modify payment Easy Social Points!';
$_['error_points']      	 = 'Exchange rate To Required!';
?>