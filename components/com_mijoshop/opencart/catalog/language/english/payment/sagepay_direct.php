<?php
/*
* @package		MijoShop
* @copyright	2009-2013 Miwisoft LLC, miwisoft.com
* @license		GNU/GPL http://www.gnu.org/copyleft/gpl.html
* @license		GNU/GPL based on AceShop www.joomace.net
*/

// Text
$_['text_title'] = 'Credit Card / Debit Card (SagePay)';
$_['text_credit_card'] = 'Card Details';
$_['text_card_type'] = 'Card Type: ';
$_['text_card_name'] = 'Card Name: ';
$_['text_card_digits'] = 'Last Digits: ';
$_['text_card_expiry'] = 'Expiry: ';
$_['text_trial'] = '%s every %s %s for %s payments then ';
$_['text_recurring'] = '%s every %s %s';
$_['text_length'] = ' for %s payments';
$_['text_loading'] = 'Loading...';

// Entry
$_['entry_card'] = 'New or Existing Card: ';
$_['entry_card_existing'] = 'Existing';
$_['entry_card_new'] = 'New';
$_['entry_card_save'] = 'Remember Card Details';
$_['entry_cc_owner'] = 'Card Owner';
$_['entry_cc_type'] = 'Card Type';
$_['entry_cc_number'] = 'Card Number';
$_['entry_cc_start_date'] = 'Card Valid From Date';
$_['entry_cc_expire_date'] = 'Card Expiry Date';
$_['entry_cc_cvv2'] = 'Card Security Code (CVV2)';
$_['entry_cc_issue'] = 'Card Issue Number';
$_['entry_cc_choice'] = 'Choose an Existing Card';

// Help
$_['help_start_date'] = '(if available)';
$_['help_issue'] = '(for Maestro and Solo cards only)';
