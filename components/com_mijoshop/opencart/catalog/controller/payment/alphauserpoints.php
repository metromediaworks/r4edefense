<?php
/*
* @package		MijoShop
* @copyright	2009-2014 Miwisoft LLC, miwisoft.com
* @license		GNU/GPL http://www.gnu.org/copyleft/gpl.html
* @license		GNU/GPL based on AceShop www.joomace.net
*/

// No Permission
defined('_JEXEC') or die('Restricted access');

class ControllerPaymentAlphauserpoints extends Controller {

	protected function index() {
		$this->language->load('payment/alphauserpoints');
				
		$order_info 			= $this->model_checkout_order->getOrder($this->session->data['order_id']);
		$this->data['total'] 	= $order_info['total'];
		$user_id        		= MijoShop::get('user')->getJUserIdFromOCustomer($order_info['customer_id'], $order_info['email']);
		
		#language
		$this->data['text_instruction'] 								= $this->language->get('text_instruction');
    	$this->data['text_payable'] 									= $this->language->get('text_payable');
		$this->data['text_address'] 									= $this->language->get('text_address');
		$this->data['text_payment'] 									= $this->language->get('text_payment');
		$this->data['alphauserpoints'] 									= $this->language->get('alphauserpoints');
		$this->data['alphauserpoints_submit'] 							= $this->language->get('alphauserpoints_submit');
		$this->data['alphauserpoints_conversation_label'] 				= $this->language->get('alphauserpoints_conversation_label');
		$this->data['alphauserpoints_conversation_desc'] 				= $this->language->get('alphauserpoints_conversation_desc');
		$this->data['alphauserpoints_conversion_rate_message'] 			= $this->language->get('alphauserpoints_conversion_rate_message');
		$this->data['alphauserpoints_total_points_needed_message'] 		= $this->language->get('alphauserpoints_total_points_needed_message');
		$this->data['alphauserpoints_current_points_situation'] 		= $this->language->get('alphauserpoints_current_points_situation');
		$this->data['alphauserpoints_not_enough_points_message'] 		= $this->language->get('alphauserpoints_not_enough_points_message');
		$this->data['alphauserpoints_total_points_deducted_message'] 	= $this->language->get('alphauserpoints_total_points_deducted_message');
		$this->data['alphauserpoints_msg_not_enough_points'] 			= $this->language->get('alphauserpoints_msg_not_enough_points');
		
		#variable
		$this->data['button_confirm'] 		= $this->language->get('button_confirm');	
		$this->data['order_id'] 			= $this->session->data['order_id'];
		$this->data['user_id'] 				= $user_id;
		$this->data['conversion'] 			= $this->config->get('alphauserpoints_points');
		$this->load->model('payment/alphauserpoints');
		$this->data['user_points'] 			= $this->model_payment_alphauserpoints->getAUPPoints($user_id);
		$this->data['continue'] 			= $this->url->link('checkout/success');
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/alphauserpoints.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/payment/alphauserpoints.tpl';
		} else {
			$this->template = 'default/template/payment/alphauserpoints.tpl';
		}
		
		$this->render(); 
	}
	
	public function confirm() {
		$this->language->load('payment/alphauserpoints');
	
		$this->load->model('checkout/order');	
		
		$this->load->model('payment/alphauserpoints');

		$priceInPoints  = $this->request->post['total'] * $this->config->get('alphauserpoints_points');
		
		$result = 0;

		if ($this->model_payment_alphauserpoints->getAUPPoints($this->request->post['user_id']) >= $priceInPoints)	{
			$description = 'MijoShop Order #' . $this->session->data['order_id'];
			$this->model_payment_alphauserpoints->bookAUPPoints($this->request->post['user_id'], -$priceInPoints, $description);			
		}		
		
		if($result) {
            $order_status_id = $this->config->get('jomsocialpoints_order_status_id');
            $comment='';
            $this->model_checkout_order->confirm($this->session->data['order_id'], $order_status_id, $comment, true);
        } else {
            $this->redirect($this->url->link('common/home'));
		}

	}
}
?>