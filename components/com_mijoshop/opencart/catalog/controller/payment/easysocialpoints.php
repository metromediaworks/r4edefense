<?php
/*
* @package		MijoShop
* @copyright	2009-2013 Miwisoft LLC, miwisoft.com
* @license		GNU/GPL http://www.gnu.org/copyleft/gpl.html
* @license		GNU/GPL based on AceShop www.joomace.net
*/

// No Permission
defined('_JEXEC') or die('Restricted access');

class ControllerPaymentEasysocialpoints extends Controller {

	protected function index() {
		$this->language->load('payment/easysocialpoints');
				
		$order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);
		$this->data['total'] = $order_info['total'];
        $user_id        = MijoShop::get('user')->getJUserIdFromOCustomer($order_info['customer_id'], $order_info['email']);
		
		$this->data['text_instruction'] 								= $this->language->get('text_instruction');
    	$this->data['text_payable'] 									= $this->language->get('text_payable');
		$this->data['text_address'] 									= $this->language->get('text_address');
		$this->data['text_payment'] 									= $this->language->get('text_payment');
		$this->data['easysocialpoints'] 								= $this->language->get('easysocialpoints');
		$this->data['easysocialpoints_submit'] 							= $this->language->get('easysocialpoints_submit');
		$this->data['easysocialpoints_conversation_label'] 				= $this->language->get('easysocialpoints_conversation_label');
		$this->data['easysocialpoints_conversation_desc'] 				= $this->language->get('easysocialpoints_conversation_desc');
		$this->data['easysocialpoints_conversion_rate_message'] 		= $this->language->get('easysocialpoints_conversion_rate_message');
		$this->data['easysocialpoints_total_points_needed_message'] 	= $this->language->get('easysocialpoints_total_points_needed_message');
		$this->data['easysocialpoints_current_points_situation'] 		= $this->language->get('easysocialpoints_current_points_situation');
		$this->data['easysocialpoints_not_enough_points_message'] 		= $this->language->get('easysocialpoints_not_enough_points_message');
		$this->data['easysocialpoints_total_points_deducted_message'] 	= $this->language->get('easysocialpoints_total_points_deducted_message');
		$this->data['easysocialpoints_msg_not_enough_points'] 			= $this->language->get('easysocialpoints_msg_not_enough_points');
		
		$this->data['button_confirm'] 	= $this->language->get('button_confirm');
		$this->data['order_id'] 		= $this->session->data['order_id'];
		$this->data['user_id'] 			= $user_id;
		$this->data['conversion'] 		= $this->config->get('easysocialpoints_points');
        $this->load->model('payment/easysocialpoints');
		$this->data['user_points'] 		= $this->model_payment_easysocialpoints->getESPoints($user_id);
		$this->data['continue'] 		= $this->url->link('checkout/success');
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/easysocialpoints.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/payment/easysocialpoints.tpl';
		} else {
			$this->template = 'default/template/payment/easysocialpoints.tpl';
		}	
		
		$this->render(); 
	}
	
	public function confirm() {
		$this->language->load('payment/easysocialpoints');
	
		$this->load->model('checkout/order');

		$this->load->model('payment/easysocialpoints');
		// Do the payment
		$user_points 		= $this->model_payment_easysocialpoints->getESPoints($this->request->post['user_id']);
		$points_charge 		= $this->request->post['total'] * $this->config->get('easysocialpoints_points');

        $result = 0;

		if ($points_charge <= $user_points)	{
			$result = $this->model_payment_easysocialpoints->updateESPoints(-$points_charge, $this->request->post['user_id']);
		}

        if($result) {
            $order_status_id = $this->config->get('easysocialpoints_order_status_id');
            $comment='';
            $this->model_checkout_order->confirm($this->session->data['order_id'], $order_status_id, $comment, true);
        }
        else {
            $this->redirect($this->url->link('common/home'));
		}

	}
}
?>