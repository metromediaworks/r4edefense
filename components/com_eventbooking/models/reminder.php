<?php
/**
 * @version        	1.6.6
 * @package        	Joomla
 * @subpackage		Event Booking
 * @author  		Tuan Pham Ngoc
 * @copyright    	Copyright (C) 2010 - 2014 Ossolution Team
 * @license        	GNU/GPL, see LICENSE.php
 */
// no direct access
defined('_JEXEC') or die();

/**
 * Event Booking Component Event Model
 *
 * @package		Joomla
 * @subpackage	Event Booking
 */
class EventBookingModelReminder extends JModelLegacy
{

	/**
	 * Constructor function	 
	 */
	function __construct()
	{
		parent::__construct();
	}

	/**
	 * Send reminder
	 */
	public static function sendReminder($numberEmailSendEachTime = 0)
	{
		$app = JFactory::getApplication();
		$db = JFactory::getDbo();
		$param = null;
		$config = EventbookingHelper::getConfig();
		if (!$numberEmailSendEachTime)
		{
			$numberEmailSendEachTime = 15;
		}
		if ($config->from_name)
		{
			$fromName = $config->from_name;
		}
		else
		{
			$fromName = $app->getCfg('fromname');
		}
		if ($config->from_email)
		{
			$fromEmail = $config->from_email;
		}
		else
		{
			$fromEmail = $app->getCfg('mailfrom');
		}
		$sql = 'SELECT a.id, a.first_name, a.last_name, a.email, a.register_date, a.transaction_id, a.language, b.id as event_id, b.title AS event_title, b.event_date ' .
			 ' FROM #__eb_registrants AS a INNER JOIN #__eb_events AS b ' . ' ON a.event_id = b.id ' .
			 ' WHERE a.published=1 AND a.is_reminder_sent = 0 AND b.enable_auto_reminder=1 AND (DATEDIFF(b.event_date, NOW()) <= b.remind_before_x_days) AND (DATEDIFF(b.event_date, NOW()) >=0) ORDER BY b.event_date, a.register_date ' .
			 ' LIMIT ' . $numberEmailSendEachTime;
		$db->setQuery($sql);
		$rows = $db->loadObjectList();
		$message = EventbookingHelper::getMessages();
		$mailer = JFactory::getMailer();
		$ids = array();
		for ($i = 0, $n = count($rows); $i < $n; $i++)
		{
			$row = $rows[$i];
			$ids[] = $row->id;
			$fieldSuffix = EventbookingHelper::getFieldSuffix($row->language);
			if (strlen($message->{'reminder_email_subject' . $fieldSuffix}))
			{
				$emailSubject = $message->{'reminder_email_subject' . $fieldSuffix};
			}
			else
			{
				$emailSubject = $message->reminder_email_subject;
			}
			$emailSubject = str_replace('[EVENT_TITLE]', $row->event_title, $emailSubject);
			if (strlen($message->{'reminder_email_body' . $fieldSuffix}))
			{
				$emailBody = $message->{'reminder_email_body' . $fieldSuffix};
			}
			else
			{
				$emailBody = $message->reminder_email_body;
			}
			$replaces = array();
			$replaces['event_date'] = JHtml::_('date', $row->event_date, $config->event_date_format, $param);
			$replaces['first_name'] = $row->first_name;
			$replaces['last_name'] = $row->last_name;
			$replaces['event_title'] = $row->event_title;
			foreach ($replaces as $key => $value)
			{
				$emailBody = str_replace('[' . strtoupper($key) . ']', $value, $emailBody);
			}
			$mailer->sendMail($fromEmail, $fromName, $row->email, $emailSubject, $emailBody, 1);
			$mailer->ClearAllRecipients();
		}
		
		if (count($ids))
		{
			$sql = 'UPDATE #__eb_registrants SET is_reminder_sent = 1 WHERE id IN (' . implode(',', $ids) . ')';
			$db->setQuery($sql);
			$db->execute();
		}
	}
} 