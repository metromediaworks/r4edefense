<?php
/**
 * @version        	1.6.6
 * @package        	Joomla
 * @subpackage		Event Booking
 * @author  		Tuan Pham Ngoc
 * @copyright    	Copyright (C) 2010 - 2014 Ossolution Team
 * @license        	GNU/GPL, see LICENSE.php
 */
// no direct access
defined( '_JEXEC' ) or die ;
?>
<h1 class="eb-page-heading"><?php echo JText::_('EB_INVITATION_COMPLETE'); ?></h1>
<p class="eb-message"><?php echo $this->message; ?></p>