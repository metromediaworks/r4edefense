<?php
// Generated during install (June 29, 2012, 1:47 pm)

// HTTP
define('HTTP_SERVER', 'localhost/shop6u/components/com_mijoshop/opencart/admin/');
define('HTTP_IMAGE', 'localhost/shop6u/components/com_mijoshop/opencart/image/');

// HTTPS
define('HTTPS_SERVER', 'localhost/shop6u/components/com_mijoshop/opencart/admin/');
define('HTTPS_IMAGE', 'localhost/shop6u/components/com_mijoshop/opencart/image/');

// DIR
define('BASE_DIR', JPATH_ROOT.'/components/com_mijoshop/opencart/');
define('DIR_MIJOSHOP', JPATH_ROOT.'/components/com_mijoshop/mijoshop/');

define('DIR_APPLICATION', JPATH_ROOT.'/components/com_mijoshop/opencart/shipstation/');
define('DIR_SYSTEM', JPATH_ROOT.'/components/com_mijoshop/opencart/system/');
define('DIR_DATABASE', JPATH_ROOT.'/components/com_mijoshop/opencart/system/database/');
define('DIR_LANGUAGE', JPATH_ROOT.'/components/com_mijoshop/opencart/admin/language/');
define('DIR_CONFIG', JPATH_ROOT.'/components/com_mijoshop/opencart/system/config/');
define('DIR_IMAGE', JPATH_ROOT.'/components/com_mijoshop/opencart/image/');
define('DIR_CACHE', JPATH_ROOT.'/components/com_mijoshop/opencart/system/cache/');
define('DIR_LOGS', JPATH_ROOT.'/components/com_mijoshop/opencart/system/logs/');


require_once(DIR_MIJOSHOP . 'mijoshop.php');
// DB
define("DB_DRIVER", 'mysql');
define("DB_HOSTNAME", MijoShop::getClass('db')->getDbAttribs('host'));
define("DB_USERNAME", MijoShop::getClass('db')->getDbAttribs('user'));
define("DB_PASSWORD", MijoShop::getClass('db')->getDbAttribs('password'));
define("DB_DATABASE", MijoShop::getClass('db')->getDbAttribs('database'));
define("DB_PREFIX", '#__mijoshop_');
?>