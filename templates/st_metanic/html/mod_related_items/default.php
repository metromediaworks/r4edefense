<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_related_items
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<div class="relateditems<?php echo $moduleclass_sfx; ?> row-fluid">
<?php foreach ($list as $item) :	?>
<?php 
$db =& JFactory::getDBO(); 
$query = "SELECT images FROM #__content WHERE id = " . $item->id ;
$db->setQuery($query); 
$images = $db->loadAssoc(); 
$image = json_decode($images['images']);

$db2 =& JFactory::getDBO(); 
$query2 = "SELECT title FROM #__categories WHERE id = " . $item->catid ;
$db2->setQuery($query2);
$category = $db2->loadAssoc();
$cat_title = $category['title'];

?>

<div class="item span4">
	<?php if(isset($image->image_intro) && $image->image_intro != ''):?>
	<div class="images">
		<a href="<?php echo $item->route;?>">
			<img src="<?php echo $image->image_intro;?>" alt="<?php $item->title;?>" />
		</a>
	</div>
	<?php endif; ?>
	<div class="link">
		<div class="title">
			<a href="<?php echo $item->route; ?>">
				<?php if ($showDate) echo JHTML::_('date', $item->created, JText::_('DATE_FORMAT_LC4')). " - "; ?>
				<?php echo $item->title; ?>
			</a>
		</div>
		<div class="category">
			<?php echo $cat_title ;?>
		</div>
	</div>
	<a class="icon-view" href="<?php echo $item->route; ?>"></a>
</div>
<?php endforeach; ?>
</div>
