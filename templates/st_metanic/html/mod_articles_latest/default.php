<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_articles_latest
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<ul class="latestnews<?php echo $moduleclass_sfx; ?>">
<?php foreach ($list as $item) :  ?>
	<li>
		<h3 class="title">
			<a href="<?php echo $item->link; ?>">
				<?php echo $item->title; ?></a>
		</h3>
		<?php if(isset($item->publish_up)): ?>
		<p class="published">
			<?php echo JHtml::_('date', $item->publish_up, JText::_('DATE_FORMAT_LC3')); ?>
		</p>
		<?php endif; ?>
	</li>
<?php endforeach; ?>
</ul>
