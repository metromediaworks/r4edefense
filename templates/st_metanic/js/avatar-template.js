(function( $ ){
	// template object
	avatarTemplate = {}
	avatarTemplate.template = { name: '', version: ''}
  	avatarTemplate.url = { base: ''}
  	avatarTemplate.menu = {}
  	avatarTemplate.image = {}
  	avatarTemplate.layout = {}
  	avatarTemplate.settingPanel = {}
  	avatarTemplate.css3effect = { options: []}
  	
  	avatarTemplate.url.getVar = function (url) 
  	{
  		var vars = {},
	    	parts = url.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) { vars[key] = value; });
	    return vars;
	}
	
	avatarTemplate.url.getBaseURL = function (url) {
		return avatarTemplate.url.base;
	}
	
  	avatarTemplate.image.initEffects = function () {
  		avatarImage.effects.overlay();
  	}
  	
  	avatarTemplate.layout.init = function () {
  		avatarLayout.functions.sameHeightCols($('#avatar-body-middle > div:not(".clearbreak")'));
  		avatarLayout.functions.leftRightCols();
  		//avatarLayout.functions.editModule();
  	}

	avatarTemplate.settingPanel.init = function () {
		avatarSettingPanel.init();
  		avatarSettingPanel.background.change('#avatar-settings .bg-image .item'); 
  	}
  	
  	avatarTemplate.menu.init = function () {
  		avatarMenu.responsive.toggleButton();	
  	}
  	
  	avatarTemplate.css3effect.init = function () {
  		avatarCSS3.init();
  	}
  	
})( jQuery );

(function( $ ){
	avatarImage = {}
	avatarImage.effects = {}
	
	avatarImage.effects.overlay = function () 
	{
		$("div[class*='img-']").each(function()
		{
			var wrap = $(this),
			wh = wrap.height();
			ww = wrap.width();
			
			wrap.find('img').each(function(){
				var img = $(this);
				ih = img.height();
				iw = img.width();
				img.css({top: (wh - ih)/2 + 'px', left : (ww - iw)/2 + 'px'});
			});
		});
	}
})( jQuery );

(function( $ ){
	avatarLayout = {}
	avatarLayout.functions = {}
	
	avatarLayout.functions.sameHeightCols = function (els) 
	{
		function calSize() 
		{
			if($(window).width() > 767) 
			{ 
				if(els.length > 0) 
				{
					var $height = 0;
					
					els.each(function()
					{
						$h = $(this).height();
						
						if ($h > $height) {
							$height = $h;
						}
					});	
					
					els.each(function(){
						$(this).css('min-height', $height);
					});
				}
			} else {
				els.each(function(){
						$(this).css('min-height', '');
				});
			}
		}
		
		$(window).load(function () {
			calSize();
		});
		
		$(window).resize(function(){
			calSize();
		});
	}
	
	avatarLayout.functions.leftRightCols = function ()
	{
		if($(window).width() > 767) 
		{
			var right = $('#avatar-right'),
			left = $('#avatar-left'),
			content = $('#avatar-content'),
			rw = 0,
			lw = 0;
			
			pw = content.parent().width();
		
			if (right.length > 0) {
				rw = right.width()/pw*100;
			}
			
			if (left.length > 0) {
				lw = left.width()/pw*100;
			}
			
			if (content.length > 0) {
				content.width((100 - rw - lw).toFixed(1) + '%');
			}	
		}
	}
	
	// avatarLayout.functions.editModule = function() {
		// $('body').append($('<div class="st-module-edit"><a id="st-module-edit">Edit</a></div>'));
// 		
		// $('.avatar-module').each(function(){
			// var id = $(this).attr('id');
			// var url = avatarTemplate.url.base + 'administrator/index.php?option=com_modules&view=module&layout=edit&id=' + id;
// 			
			// $(this).hover(function(){
				// $('#st-module-edit').attr({'href': url, 'target': ''});
				// var mP = $(this).position();
// 				
				// $('#st-module-edit').css({
					// 'position': 'absolute',
					// 'z-index': '99999',
					// 'left': mP.left,
					// 'top': mP.top
				// });
// 				
			// });
		// });
	// }
})( jQuery );

(function( $ ){
	avatarSettingPanel = {}
	avatarSettingPanel.background = {}
	avatarSettingPanel.header = {}
	avatarSettingPanel.body = {}
	avatarSettingPanel.link = {}
	avatarSettingPanel.showcase = {}
	
	avatarSettingPanel.init = function () 
	{
		var panelSetting = $('#avatar-settings');
		$('#avatar-settings #close').click(function()
		{
			if (panelSetting.css('left') == '0px') {
				panelSetting.animate({ left: '-172px'}, 300);	
			} else {
				panelSetting.animate({ left: '0px'}, 300);
			}
		});
	}
	
	avatarSettingPanel.reset = function () 
	{
		var allCookies = document.cookie.split(';');
		
		for (var i=0;i<allCookies.length;i++) 
		{
			var cookiePair = allCookies[i].split('=');
			if (cookiePair[0].indexOf(avatarTemplate.template.name) != -1) {
				document.cookie = cookiePair[0] + '=;path=/';
				window.location.reload();
			}
		}
	}
	
	avatarSettingPanel.background.change = function (selector) 
	{
		$(selector).each(function ()
		{
			var self = $(this);
			self.click (function()
			{
				var bg = self.css('background-image');
				
				if (/opera/i.test(navigator.userAgent)){
					bg =  encodeURIComponent(bg);
				}
				
				document.cookie = avatarTemplate.template.name + '-background-image' + '=' + bg + ';path=/';
				$('body').css('background-image', self.css('background-image'));	
			});	
		});	
	}
	
	avatarSettingPanel.header.color = function (selector) 
	{
		$(':header').each (function () {
			$(this).css('color', selector.value);	
		});
		document.cookie = avatarTemplate.template.name + '-header-color' + '=' + selector.value + ';path=/';
	}
	
	avatarSettingPanel.body.color = function (selector) 
	{
		$('body').css('color', selector.value);	
		document.cookie = avatarTemplate.template.name + '-body-color' + '=' + selector.value + ';path=/';
	}
	
	avatarSettingPanel.showcase.change = function (selector) 
  	{
  		var self = avatarTemplate;
		document.cookie = avatarTemplate.template.name + '-showcase' + '=' + selector + ';path=/';
		window.location.reload();
   	}
})( jQuery );

(function( $ ){
	avatarMenu = {};
	avatarMenu.responsive = {};
	// avatarMenu.megaMenu = {}
	
	avatarMenu.responsive.toggleButton = function (){
		$('.avatar-nav-responsive .toggle').click(function()
		{
			var toggle = $(this);
			var parent = toggle.parent();

			parent.find('ul.st-mega-menu').slideToggle();			

			$(window).resize(function(){
				if($(window).width() > 1024) {
					parent.find('ul.st-mega-menu').removeAttr('style');
				}
			});
		});
		
		if ($('body').hasClass('avatar-responsive'))
		{
			var mainMenu = $('.avatar-nav-responsive > ul');
			
			mainMenu.find('li > span.pull').click(function(event){
				$(this.getParent()).find('div.st-mega-menu-row:first').slideToggle();
				$(this).toggleClass('active');
			});
			
			$(window).resize(function(){
				if($(window).width() > 1024) {
					mainMenu.find('div.st-mega-menu-row').removeAttr('style');
				}
			});
		};
		
	};
	// avatarMenu.megaMenu.init = function () {
		// $('.avatar-main-menu > li').each(function(){
			// var li  = $(this);
// 			
			// li.hover(function(){
				// var wW = $(window).width();
				// var mega = $(this).find(".st-menu-mega");
				// mega.each(function(){
					// wM = $(this).width();
					// leftLi = li.position();
					// if (leftLi.left + wM > wW) {
						// $(this).parent().parent().css('left', '-' + (leftLi.left - ((wW - wM) /2)) + 'px');
					// } else {
						// $(this).parent().parent().css('left', -(leftLi.left - (wM/2)) + 'px');	
					// }
				// });
			// },
			// function (){
				// var mega = $(this).find(".st-menu-mega");
				// mega.each(function(){
					// $(this).parent().parent().css('left', "-9999px");
				// });
			// });
		// });
	// }
})( jQuery );

(function( $ ){
	avatarCSS3 = {};
	avatarCSS3.appearAfterScroll = {};
	
	avatarCSS3.appearAfterScroll.isIntoView = function(elem, ratio) 
	{
		var docViewTop = $(window).scrollTop();
	    var docViewBottom = docViewTop + $(window).height();
		var elemTop = $(elem).offset().top;
		var elemBottom = elemTop + $(elem).height();
		
		return (elemTop <= docViewBottom - ratio);	
	};
	
	avatarCSS3.appearAfterScroll.init = function() 
	{
		avatarCSS3.appearAfterScroll.checkIntoView(true);
		$(window).scroll(function() {
			avatarCSS3.appearAfterScroll.checkIntoView(false); 	
		}); 
	};
	
	avatarCSS3.appearAfterScroll.checkIntoView = function(start) {
		if (!avatarTemplate.template.params.css3_effect_scroll) {
			return;
		};
		var selector = avatarTemplate.template.params.css3_effect_scroll.split(';');
		selector.push('.avatar-module');
		
		$.each(selector, function(index, value) {
			if (value) 
			{
				var els = $(value);
				
				if (els.length > 0) {
					els.each(function(){
						var el = $(this);
						if (start) 
						{
							el.addClass('avatar-scroll-disappear');
							if (avatarCSS3.appearAfterScroll.isIntoView(el, 150)) {
								setTimeout(function(){
									el.removeClass('avatar-scroll-disappear').addClass('avatar-scroll-appear');
								}, 2000);
							} 
							// setTimeout(function(){
								// el.removeClass('avatar-scroll-disappear').addClass('avatar-scroll-appear');
							// }, 1000);
						} 
						else 
						{
							if (avatarCSS3.appearAfterScroll.isIntoView($(this), 150)) {
								$(this).addClass('avatar-scroll-appear').removeClass('avatar-scroll-disappear');
							} else {
								//$(this).addClass('avatar-scroll-disappear').removeClass('avatar-scroll-appear');
							}	
						};
					});		
				}
			}
		});	
	};
	
	avatarCSS3.init = function() 
	{
		avatarCSS3.appearAfterScroll.init();
	};
})( jQuery );


(function( $ ){
	getYouTubeUrl = function(url){
		var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=)([^#\&\?]*).*/;
		var match = url.match(regExp);
		if (match && match[2].length == 11) {
			return match[2];
		} else {
			return false;
		}
	};
})( jQuery );

(function( $ ){
	resizeVideo = function(ele,ratio){
		var 
			new_width = ele.width(),
			new_height = ele.height();
			if(!ratio){
				ratio = 16/9;
			}
			if(new_width /(new_height*ratio) < 1){
				margin_left = ( new_width - Math.ceil(new_height*ratio) ) / 2 ,
				margin_top = 0 ,
				new_width = Math.ceil(new_height*ratio) ;
			}else{
				margin_left = 0,
				margin_top = ( new_height - Math.ceil(new_width/ratio) ) / 2 ,
				new_height = Math.ceil(new_width/ratio) ;
			}
		return {
			new_width: new_width,
			new_height: new_height,
			margin_left: margin_left,
			margin_top: margin_top
		};
	};
})( jQuery );


/*=========== CUSTOM JS =========*/
(function( $ ){
	$(window).load(function(){
		$('body').animate({
			opacity: '1'
		}, 400);
		
		//alert($(window).width());
		//Resize video youtube bg
		var bg_video_top = resizeVideo($(".st-welcome-background-video .background-video-top"),16/9),
			iframe_bg_video = $(".st-welcome-background-video .background-video-top iframe");
			
			iframe_bg_video.height(bg_video_top.new_height).width(bg_video_top.new_width).css({'margin-left':bg_video_top.margin_left,'margin-top':bg_video_top.margin_top});
		
		//Resize video youtube section
		var video_section = resizeVideo($('.st-video-section .video-section') , 16/9),
			iframe_video_section = $('.st-video-section .video-section iframe');
		
			iframe_video_section.height(video_section.new_height).width(video_section.new_width).css({'margin-left':video_section.margin_left,'margin-top':video_section.margin_top});
			
			//alert(iframe_video_section.selector);
			//console.log(iframe_video_section);
		$(window).resize(function(){
			
			//Resize video youtube bg
		var bg_video_top = resizeVideo($(".st-welcome-background-video .background-video-top"),16/9),
			iframe_bg_video = $(".st-welcome-background-video .background-video-top iframe");
			
			iframe_bg_video.height(bg_video_top.new_height).width(bg_video_top.new_width).css({'margin-left':bg_video_top.margin_left,'margin-top':bg_video_top.margin_top});
		
		//Resize video youtube section
		var video_section = resizeVideo($('.st-video-section .video-section') , 16/9),
			iframe_video_section = $('.st-video-section .video-section iframe');
		
			iframe_video_section.height(video_section.new_height).width(video_section.new_width).css({'margin-left':video_section.margin_left,'margin-top':video_section.margin_top});
		});
		
		//Set play video when load page
		//$('.st-video-section .video-section iframe .ytp-large-play-button').trigger('click');
		
	});
	$(document).ready(function(){	
		
		//parallaxInit();
		$('.st-parallax-top').parallax("50%", 0.6);
		$('.st-parallax-our-services').parallax("50%",0.6);
		$('.st-parallax-ready-to-stand-out').parallax("50%",0.6);
		$('.st-parallax-buy-this-theme').parallax("50%",0.6);
		$('.st-parallax-contact').parallax("50%",0.6);
		$('.st-parallax-we-work-hard').parallax("50%",0.6);
		
		
		//background video top - youtube video
		var bg_video_top_id = getYouTubeUrl($('.st-welcome-background-video .background-video-top>p').text());
		$(".st-welcome-background-video .background-video-top").tubeplayer({
			width: '100%', //bg_video_top_WH.newWidth,
			height: '100%', //bg_video_top_WH.newHeight,
			initialVideo: bg_video_top_id,
		});
		
		//video section - youtube video 
		var video_section_id = getYouTubeUrl($('.st-video-section .video-section>p').text());
		$('.st-video-section .video-section').tubeplayer({
			width: '1920px',
			height: '510px',
			initialVideo: video_section_id,
			autoPlay: 0
		});
		
		
		if($('.st-page-hello #avatar-header-inside-block').length >0){
			var hello_menuTop = $('.st-page-hello #avatar-header-inside-block').offset().top,
				hello_menuHeight = $('.st-page-hello #avatar-header-inside-block').height();
		}
		
		//Addclass for .text contact form hello page
		if($('.st-form-contact .row-fluid>div.text').length >0){
			$('.st-form-contact .row-fluid>div.text').addClass('span4');
		}
		
		var j = 0;
		//Add class for .st-page-hello #avatar-header-inside-block
		$(window).scroll(function(){
			var windowScroll = $(window).scrollTop();
			winH = $(window).height();
			if(windowScroll - hello_menuTop  >= 0){
				$('.st-page-hello #avatar-header-inside-block').addClass('scroll-down');	
			}else{
				$('.st-page-hello #avatar-header-inside-block').removeClass('scroll-down');
				$('.st-page-hello #avatar-pos-top-right ul.st-mega-menu>li').removeClass('current').removeClass('active');
				$('.st-page-hello #avatar-pos-top-right ul.st-mega-menu>li>a.home-hello').parent().addClass('current').addClass('active');
				
			}; 
			
			var number = $('.st-our-skill .st-border>.value');
			if(number.length > 0){
				var number_top = number.first().offset().top;
				if($('.st-our-skill').hasClass('avatar-scroll-appear') && j<1 && (number_top - winH < windowScroll)){
					j++;
					$.each(number, function(index, value){
						var $this = number.eq(index);
						var num = parseInt($this.text());
						var i = 1;
						var number_count = setInterval(function(){
							i ++;
							var text=parseInt((num/50)*i)+'&#37;';
							$this.html(text);
							if(parseInt($this.text())>=num){
								clearInterval(number_count);
							}
						},60);
					});
				};
			};
			
		});
		
		// one page effects
		var docViewTop = $(window).scrollTop();
		var wH = $(window).height();
	    var docViewBottom = docViewTop + $(window).height();
		
		$elements = $('#avatar-pos-top-right ul.st-mega-menu>li>a:not(.home-hello)');
		
		$('.st-page-hello #avatar-pos-top-right ul.st-mega-menu>li>a.home-hello').click(function(event){
			event.preventDefault();
			//console.log(hello_menuTop);
			$('body,html').animate({
				scrollTop: 0
			},800,'easeInExpo');
		});
		
		$.each($elements, function(index, value){
				
				if (!$(value).attr('class')) {
					return;
				}
				className = $.trim($(value).attr('class').replace('onepage-disappear', '').replace('onepage-appear', ''));
				var ele = $(className);
				
				if (!ele.length) {
					return;
				}
		});
		
		$.each($elements, function(index, value){
			var ele = $(value);
			//var selector = ele.attr('class');
			ele.click(function(event){
				if (!$(this).attr('class')) {
					return;
				}
				
				selector = $.trim($(this).attr('class').replace('onepage-disappear', '').replace('onepage-appear', ''));
				ele = $(selector);
				
				if (!ele.length) {
					var url = $('#avatar-pos-top-right ul.menu li a.home-hello').attr('href');
					window.location = url;
				}
				
				event.preventDefault();
				
				var elH = ele.height();
				var menuH = $('#avatar-header-inside-block').height();
				
				if( $('body').hasClass('st-page-hello') ){
					if($('#avatar-header-inside-block').hasClass('scroll-down')){
						$('body,html').animate({
							scrollTop: ele.offset().top - menuH 
						}, 800, 'easeInExpo',function(){
						});
					}else{
						$('body,html').animate({
							scrollTop: ele.offset().top - menuH - 88
						}, 800 ,'easeInExpo', function(){
						});
					};
				}else{
					$('body,html').animate({
						scrollTop: ele.offset().top
					}, 800 ,'easeInExpo', function(){
					});
				};
				
				
				$.each($elements, function(index, value){
					className = $.trim($(this).attr('class'));
					//.replace('onepage-disappear', '').replace('onepage-appear', ''));
					$(className).not(selector).removeClass('onepage-appear').addClass('onepage-disappear');		
				});
			});
		});

		$(window).scroll(function() {
			$.each($elements, function(index, value){
				
				if (!$(this).attr('class')) {
					return;
				}
				className = $.trim($(this).attr('class').replace('onepage-disappear', '').replace('onepage-appear', ''));
				var ele = $(className);
				
				if (!ele.length) {
					return;
				};
				
				var menuH = $('#avatar-header-inside-block').height();
				
				if (avatarCSS3.appearAfterScroll.isIntoView(ele, 300)) {
					ele.addClass('onepage-appear').removeClass('onepage-disappear');
					//Add active current for menu
					//if( ele.offset().top + ele.height() -$(window).scrollTop() >=0 ){
						//console.log('hi');
						//ele.offset().top + wH - ele.height() - $(window).scrollTop() >= 0
					//};
					if((ele.offset().top + wH - ele.height() - $(window).scrollTop() > -5) && ($(window).scrollTop() - ele.offset().top + menuH > -5) && ($('body').hasClass('st-page-hello'))){
						
						$(this).parent().siblings().removeClass('current').removeClass('active');
						$(this).parent().addClass('current').addClass('active');
					}
					
				} else {
					ele.addClass('onepage-disappear').removeClass('onepage-appear');
				}	
			});
		});
		
		$('.avatar-main-menu li').each(function(){
			$(this).click(function(){
				
				$(this).addClass('current').addClass('active');
				
				$(this).siblings().each(function(){
					$(this).removeClass('current').removeClass('active');
				});
			});
		});
		
		//Set play - pause video section
		$('.st-video-section-title p a.video-icon').click(function(){
			event.preventDefault();
			if($(this).hasClass('fa-play')){
				$('.st-video-section .video-section ').tubeplayer('play');
			}else{
				$('.st-video-section .video-section').tubeplayer('pause');
			};
			$(this).toggleClass('fa-play').toggleClass('fa-pause');			
		});
		
	});
})(jQuery);
